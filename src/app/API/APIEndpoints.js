window.API="http://173.249.49.7:9020/api/"

export let BASE_URL = window.API;
export let CATEGORY ="category"
export let CITIES ="cities/"
export let STATES ="states"
export let SUBCATEGORY="sub-category"
export let BULKUPLOADFILE="bulk-upload-file"
export let PRODUCTS ="products"
export let FROMEXCEL ="from-excel"
export let GETBULKREPORT ="/get-bulk-report"
export let ROLES ="roles"
