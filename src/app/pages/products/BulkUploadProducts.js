import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import {Link} from 'react-router-dom'
import clsx from "clsx";
import { BASE_URL, BULKUPLOADFILE, PRODUCTS, FROMEXCEL } from '../../API/APIEndpoints';
import APIServices from '../../API/APIServices';

export default function BulkUploadProducts() {
    const useStyles = makeStyles(theme => ({
        button: {
          margin: theme.spacing(1),
          textAlign:'end'
        },
        input: {
          display: 'none',
          padding:'2% 0'
        },
      }));

      const [file,setFile]=useState(null)
      const [fileName,setFileName]=useState(null) 
      const [loading, setLoading] = useState(false);
      const [uploadButtonStyle, setUploadButtonStyle] = useState({
        paddingRight: "2.5rem",
        opacity:'1'
      });
      const enableLoading = () => {
        setLoading(true);
        setUploadButtonStyle({ paddingRight: "3.5rem",opacity:'0.6' });
      };
      const disableLoading = () => {
        setLoading(false);
        setUploadButtonStyle({ paddingRight: "2.5rem",opacity:'1' });
      };
      const [fileData,setFileData]=useState([])

      const onFileChange=(e)=>{
        setFile(e.target.files[0])
        setFileName(e.target.files[0].name)
      }

      const uploadFile=()=>{
        enableLoading();
        setTimeout(async() => {
          disableLoading()
          const data = new FormData()
          data.append('avatar',file,file.name)

          const url =BASE_URL + BULKUPLOADFILE
          const bulkUpload = await new APIServices().post(url,data,'form-data')
          console.log(bulkUpload)
          if(bulkUpload.status===200){
            const data = bulkUpload.results.uploadFile
            const fileExtension= data.path.split('.');
            const uploadFileData={
              fileId:data.id,
              fileExtension:fileExtension[fileExtension.length-1],
              path:data.path
            }

            const url = BASE_URL + PRODUCTS +'/' + FROMEXCEL

            const bulkUploadData = await new APIServices().post(url,uploadFileData)

            console.log(bulkUploadData)
          }
        }, 1000);
      }
      
    const classes = useStyles();
    return (
        <div className="bulkMain">
          <div style={{textAlign:'end'}}>
            <Link to="/products">
                <Button variant="contained" color="primary" className={classes.button}>
                  Show Products
                </Button>
            </Link>
          </div>

          <div className="bulkButton">
            <h3>Bulk Upload Product</h3>

            <p className="bulkMsg">To upload on worksheet here click on attach Excel File.</p>

            <div style={{padding:'1% 0'}}>
              <input
                accept=".xlsx, .xls, .csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excels"
                className={classes.input}
                id="contained-button-file"
                type="file"
                onChange={onFileChange}
              />
              <label htmlFor="contained-button-file">
                <Button variant="outlined" color="primary" component="span">
                  ATTACH EXCEL FILE
                </Button>
              </label>
            <p>{fileName}</p>
            </div>

            <button
              id="kt_login_signin_submit"
              type="submit"
              onClick={!loading ? uploadFile : null}
              className={`btn btn-primary btn-elevate kt-login__btn-primary ${clsx(
                {
                  "kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light": loading
                }
              )}`}
              style={uploadButtonStyle}
            >
              Upload
            </button>
            
          </div>
        </div>
    )
}
