import React, { useState, useEffect } from 'react'
import { BASE_URL, SUBCATEGORY } from '../../API/APIEndpoints'
import APIServices from '../../API/APIServices'
import TableReuse from '../home/table/TableReuse'
import { TableHead, TableRow, TableCell, Paper, TablePagination, TableBody } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import DeleteOutlineOutlinedIcon from '@material-ui/icons/DeleteOutlineOutlined';
import Delete from '../../modal/Delete'


const useStyles = makeStyles((theme)=>({
    paper: {
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
        position:"relative"
    },
}));

export default function SubCategoryTable(props) {
    const classes = useStyles();
    const [page, setPage] = React.useState(0);
    const [countData,setCountData]=useState(0)
    const [rowsPerPage, setRowsPerPage] = React.useState(10);
    const [subCategory,setSubCategory]=useState([])
    const [columns]=useState([
        {id:1,name:'Sub Category'},
        {id:2,name:'Category'},
        {id:3,name:'Action'}
    ])
    const [deleteAPI,setDelete]=useState()
    const [deleteId,setDeleteId]=useState()
    const [open,setOpen]=useState(false)

    async function fetchSubCategory(){
        const url=BASE_URL +SUBCATEGORY +`?from=${page * rowsPerPage+1}&to=${page * rowsPerPage + rowsPerPage}`
        const subCategoryMain = await new APIServices().get(url)
        const subCategoryData = subCategoryMain.results.data
        const count = subCategoryMain.results.count
        setSubCategory(subCategoryData)
        setCountData(count)
    }

    useEffect(() => {
        fetchSubCategory()
    }, [props.add,props.updateData,deleteAPI,page])

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };
    
    const handleChangeRowsPerPage = event => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };

    const deleteSubCategory=(id)=>{
        setDeleteId(id)
        setOpen(!open)
    }

    const deleteConfirm=async()=>{
        const url = BASE_URL+SUBCATEGORY+'/'+deleteId
        const deleteSubCategory = await new APIServices().delete(url)
        setDelete(deleteSubCategory)
        setOpen(!open)
    }

    const handleClick=()=>{
        setOpen(!open)
    }
    console.log(subCategory)
    return (
        <>
        {subCategory.length>0 &&
            <div>
                <TableReuse>
                    <TableHead>
                        <TableRow>
                            {columns.map((data,i)=>(
                                <TableCell key={i}>
                                    {data.name}
                                </TableCell>
                            ))}
                        </TableRow>
                    </TableHead>
                    
                    <TableBody>
                        {subCategory.map(data =>{
                            return(
                            <TableRow key={data.id}>
                                <TableCell>{data.subCategoryName}</TableCell>
                                <TableCell>{data.category.categoryName}</TableCell>
                                <TableCell className='flex cursor'>
                                    <EditOutlinedIcon onClick={()=>props.editSubCategory(data)}/>
                                    <DeleteOutlineOutlinedIcon onClick={()=>deleteSubCategory(data.id)} />
                                </TableCell>
                            </TableRow>
                            )
                        })}
                    </TableBody>
                </TableReuse>
                <Paper className={classes.boxShadow}>
                    <TablePagination
                        rowsPerPageOptions={[10, 25, 100]}
                        component="div"
                        count={countData}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        onChangePage={handleChangePage}
                        onChangeRowsPerPage={handleChangeRowsPerPage}
                    />
                </Paper>

                {open &&
                    <Delete
                        open={open}
                        clickConfirm={deleteConfirm}
                        confirmMsg="Are you sure you want to delete this Sub Category?"
                        handleClick={handleClick}
                    />
                }
            </div>
        }
        </>
    )
}
