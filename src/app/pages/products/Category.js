import React, { useEffect, useState } from 'react'
import { makeStyles } from '@material-ui/core/styles';
import { Formik } from 'formik'
import { BASE_URL, CATEGORY } from '../../API/APIEndpoints';
import TableReuse from '../home/table/TableReuse';
import { Backdrop,TableHead, TableRow, TableCell, TableBody, Paper, TablePagination, TextField, Modal, Fade, Button } from '@material-ui/core';
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import ClearRoundedIcon from '@material-ui/icons/ClearRounded';
import DeleteOutlineOutlinedIcon from '@material-ui/icons/DeleteOutlineOutlined';
import * as Yup from 'yup'
import clsx from "clsx";
import ErrorValidation from '../errors/ErrorValidation';
import APIServices from '../../API/APIServices';
import Delete from '../../modal/Delete';

const useStyles = makeStyles((theme)=>({
    boxShadow:{
        boxShadow:"none !important"
    },
    flex:{
        display:'flex',
        cursor:'pointer'
    },
    margin:{
        marginRight:'10px'
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        background:'rgba(0,0,0,0.6)',
        textAlign:'center'
      },
      paper: {
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
        position:"relative"
      },
  }));

export default function Category() {
    //     STATE      //
    const classes = useStyles();
    const [page, setPage] = React.useState(0);
    const [countData,setCountData]=useState(0)
    const [rowsPerPage, setRowsPerPage] = React.useState(10);
    const [columns]=useState([
        {name:'Category'},
        {name:'Action'}
    ])
    const [category,setCategory]=useState([])
    const [loadingButtonStyle, setLoadingButtonStyle] = useState({
        paddingRight: "2.5rem"
      });
    const [loading, setLoading] = useState(false);
    const [open, setOpen] = React.useState(false);
    const [editData,setEditData]=useState()
    const [editCategoryData,setEditCategoryData]=useState(false)
    const [deleteOpen,setDeleteOpen] = useState(false)
    const [deleteId,setDeleteId]=useState()

    const handleOpen = () => {
        setOpen(!open);
        setEditCategoryData(false)
    };
      
      const enableLoading = () => {
        setLoading(true);
        setLoadingButtonStyle({ paddingRight: "3.5rem" });
      };
    
      const disableLoading = () => {
        setLoading(false);
        setLoadingButtonStyle({ paddingRight: "2.5rem" });
      };

    // GET CATEGORY API //
      
    async function fetchAPI(){
        const url = BASE_URL + CATEGORY + `?from=${page * rowsPerPage+1}&to=${page * rowsPerPage + rowsPerPage}`
        const categoryData = await new APIServices().get(url)
        const count = categoryData.results.count
        const dataCategory = categoryData.results.data;
        setCategory(dataCategory)
        setCountData(count)
    }
    useEffect(()=>{
        fetchAPI();
    },[page])


    //  PAGINTION //
    const handleChangePage = (event, newPage) => {
        setPage(newPage);
      };
    
      const handleChangeRowsPerPage = event => {
        setRowsPerPage(+event.target.value);
        setPage(0);
      };

    // VALIDATION //
    const validationSchema = Yup.object().shape({
        categoryName:Yup.string()
        .required("Category Name is Required")
    })
    console.log(localStorage.getItem("apitoken"))

    const deleteCategory = (id)=>{
        setDeleteOpen(!deleteOpen)
        setDeleteId(id)
    }

    const deleteConfirm = async()=>{
        const deleteCategoryUrl = BASE_URL + CATEGORY +'/'+deleteId
        const deleteCategory = await new APIServices().delete(deleteCategoryUrl)
        console.log(deleteCategory)
        fetchAPI()
        setDeleteOpen(!deleteOpen)
    }

    const handleClick=()=>{
        setDeleteOpen(!deleteOpen)
    }

    const editCategory = (category_data)=>{
        setOpen(true)
        setEditData(category_data)
        setEditCategoryData(true)
    }

    console.log(page * rowsPerPage,page * rowsPerPage + rowsPerPage)
    console.log(page * rowsPerPage)
    console.log((page+1) * rowsPerPage)
    console.log(category)
    return (
        <div>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={open}
                onClose={handleOpen}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                timeout: 500,
                }}
            >
                <Fade in={open}>
                    <div className={classes.paper}>
                        <div className="closeIcon" onClick={handleOpen}>
                            <ClearRoundedIcon/>
                        </div>
                        <h4 style={{margin:'15px 0'}}>{editCategoryData ? "Update Category":"Add Category"}</h4>
                        <Formik
                            initialValues={{categoryName:editCategoryData ? editData.categoryName:''}}
                            validationSchema={validationSchema}
                            className={classes.paper}

                        onSubmit={async(values, { setSubmitting,resetForm }) => {
                            enableLoading();
                            setSubmitting(true)

                            {!editCategoryData ?
                            setTimeout(async()=>{
                                const categoryPostUrl = BASE_URL + CATEGORY
                                const categoryPostData = {
                                    categoryName:values.categoryName,
                                    createdby:2
                                }
                                const postCategory=await new APIServices().post(categoryPostUrl,categoryPostData)
                                // console.log(postCategory)
                                if(postCategory.error){
                                    setSubmitting(false)
                                    disableLoading()
                                }else{
                                    disableLoading()
                                    resetForm()
                                    setOpen(false)
                                    fetchAPI()

                                    return(
                                        <p>add</p>
                                    )
                                }
                            },1000):

                            setTimeout(async()=>{
                                const categoryUpdateUrl = BASE_URL + CATEGORY + '/' + editData.id
                                const categoryUpdateData = {
                                    categoryName:values.categoryName,
                                    createdby:2
                                }
                                const categoryUpdate = await new APIServices().patch(categoryUpdateUrl,categoryUpdateData)
                                if(categoryUpdate.error){
                                    setSubmitting(false)
                                    disableLoading()
                                }else{
                                    disableLoading()
                                    resetForm()
                                    setOpen(false)
                                    fetchAPI()
                                }
                                console.log(categoryUpdate)
                            })}
                        }}  
                    >
                        {({values,errors,touched,handleChange,handleBlur,handleSubmit,isSubmitting})=>(
                            <form
                                autoComplete="off"
                                className="kt-form"
                                onSubmit={handleSubmit}
                            >
                                <div className="form-group">
                                    <TextField
                                        type="categoryName"
                                        label="Category Name"
                                        margin="normal"
                                        className={touched.categoryName && errors.categoryName ? "has-error kt-width-full":"kt-width-full"}
                                        name="categoryName"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.categoryName}
                                    />
                                </div>
                                <ErrorValidation touched={touched.categoryName} message={errors.categoryName} />

                                <button
                                    id="kt_login_signin_submit"
                                    type="submit"
                                    disabled={isSubmitting}
                                    className={`btn btn-primary btn-elevate kt-login__btn-primary ${clsx(
                                    {
                                        "kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light": loading
                                    }
                                    )}`}
                                    style={loadingButtonStyle}
                                >
                                    {editCategoryData ? "Update":"Add"}
                                </button>           
                            </form>
                        )}
                    </Formik>
                </div>
            </Fade>
        </Modal>

        <div onClick={handleOpen} className="textRight">
            <Button variant="contained" color="primary">
                Add Category
            </Button>
        </div>
            
            {category.length>0 &&
            <div>
                <TableReuse>
                    <TableHead>
                        <TableRow>
                            {columns.map((data,i)=>(
                                <TableCell key={i}>
                                    {data.name}
                                </TableCell>
                            ))}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {category.map(data =>{
                            return(
                                <TableRow key={data.id}>
                                    <TableCell>{data.categoryName}</TableCell>
                                    <TableCell>
                                        <div className='flex cursor'>
                                            <div onClick={()=>editCategory(data)}>
                                                <EditOutlinedIcon className={classes.margin} />
                                            </div>
                                            <div onClick={()=>deleteCategory(data.id)}>
                                                <DeleteOutlineOutlinedIcon/>
                                            </div>
                                        </div>
                                    </TableCell>
                                </TableRow>
                            )
                        })}
                    </TableBody>
                </TableReuse>
                <Paper className={classes.boxShadow}>
                    <TablePagination
                        rowsPerPageOptions={[10, 25, 100]}
                        component="div"
                        count={countData}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        onChangePage={handleChangePage}
                        onChangeRowsPerPage={handleChangeRowsPerPage}
                    />
                </Paper>

                {deleteOpen &&
                    <Delete
                        open={deleteOpen}
                        clickConfirm={deleteConfirm}
                        confirmMsg="Are you sure you want to delete this Category?"
                        handleClick={handleClick}
                    />
                }
            </div>}
        </div>
    )
}
