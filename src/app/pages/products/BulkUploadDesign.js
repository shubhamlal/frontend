import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import clsx from "clsx";

export default function BulkUploadDesign() {
    const useStyles = makeStyles(theme => ({
        button: {
          margin: theme.spacing(1),
          textAlign:'end'
        },
        input: {
          display: 'none',
          padding:'2% 0'
        },
      }));

      const [file,setFile]=useState(null)
      const [fileName,setFileName]=useState(null) 
      const [loading, setLoading] = useState(false);
      const [uploadButtonStyle, setUploadButtonStyle] = useState({
        paddingRight: "2.5rem",
        opacity:'1'
      });
    
      const enableLoading = () => {
        setLoading(true);
        setUploadButtonStyle({ paddingRight: "3.5rem",opacity:'0.6'  });
      };
    
      const disableLoading = () => {
        setLoading(false);
        setUploadButtonStyle({ paddingRight: "2.5rem",opacity:'1'  });
      };

      const onFileChange=(e)=>{
        setFile(e.target.files[0])
        setFileName(e.target.files[0].name)
      }

      const fileUpload=()=>{
        const data = new FormData()
        data.append('file',{file})
      }

      const uploadFile=()=>{
        enableLoading();
        setTimeout(() => {
         disableLoading()
        }, 1000);
      }
      
    const classes = useStyles();
    return (
        <div className="bulkMain">
          
          <div className="bulkButton">
            <h3>Upload Design</h3>

            <p className="bulkMsg">To upload on your design here click on attach Excel File.</p>

            <div style={{padding:'1% 0'}}>
              <input
                accept=".xlsx, .xls, .csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excels"
                className={classes.input}
                id="contained-button-file"
                type="file"
                onChange={onFileChange}
              />
              <label htmlFor="contained-button-file">
                <Button variant="outlined" color="primary" component="span" onClick={fileUpload}>
                  ATTACH EXCEL FILE
                </Button>
              </label>
            <p>{fileName}</p>
            </div>

            <p>Note: The design should be upload as per the name</p>

            <button
              id="kt_login_signin_submit"
              type="submit"
              onClick={!loading ? uploadFile:null}
              className={`btn btn-primary btn-elevate kt-login__btn-primary ${clsx(
                {
                  "kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light": loading 
                }
              )}`}
              style={uploadButtonStyle}
            >
              Upload
            </button>
            
          </div>
        </div>
    )
}
