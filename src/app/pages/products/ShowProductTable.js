import React, { useState, useEffect } from 'react'
import TableReuse from '../home/table/TableReuse'
import { makeStyles } from '@material-ui/core/styles';
import { BASE_URL, PRODUCTS } from '../../API/APIEndpoints';
import APIServices from '../../API/APIServices';
import { TableHead, TableRow, TableCell, TableBody, Paper, TablePagination } from '@material-ui/core';
import VisibilityOutlinedIcon from '@material-ui/icons/VisibilityOutlined';
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import DeleteOutlineOutlinedIcon from '@material-ui/icons/DeleteOutlineOutlined';
import Delete from '../../modal/Delete';

const useStyles = makeStyles((theme)=>({
    paper: {
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
        position:"relative"
    },
}));

export default function ShowProductTable(props) {
    const classes = useStyles();
    const [page, setPage] = React.useState(0);
    const [countData,setCountData]=useState(0)
    const [rowsPerPage, setRowsPerPage] = React.useState(10);
    const [product,setProduct]=useState([])
    const [id,setId] =useState()
    const [open,setOpen]=useState(false)
    const [deleteProductData,setDeleteProduct] = useState()
    const [columns]=useState([
        {id:1,name:"Design Image"},
        {id:2,name:"SKU Code"},
        {id:3,name:"Product Name"},
        {id:4,name:"Category"},
        {id:5,name:"Sub Category"},
        {id:6,name:"Weight"},
        {id:7,name:"Purity"},
        {id:8,name:"Karat"},
        {id:9,name:"Jewellery Type"},
        {id:10,name:"Price"},
        {id:11,name:"Action"},
    ])

    async function fetchProduct(){
        const url = BASE_URL + PRODUCTS+`?from=${page * rowsPerPage+1}&to=${page * rowsPerPage + rowsPerPage}`
        const productData= await new APIServices().get(url)
        const products=productData.results.data
        const count = productData.results.count
        setProduct(products)
        setCountData(count)
    }

    useEffect(()=>{
        fetchProduct()
    },[deleteProductData,props.updateData,page])
    
    const deleteProduct =(id)=>{
        setId(id)
        setOpen(!open)
    }

    const deleteConfirm =async()=>{
        const url = BASE_URL + PRODUCTS +'/'+id
        const deleteProduct = await new APIServices().delete(url)
        setDeleteProduct(deleteProduct)
        setOpen(!open)
    }

    const handleClick=()=>{
        setOpen(!open)
    }
    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };
    
    const handleChangeRowsPerPage = event => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };
    return (
        <>
            {product.length>0 &&
                <div>
                    <TableReuse>
                        <TableHead>
                            <TableRow>
                                {columns.map(data =>(
                                    <TableCell key={data.id}>
                                        {data.name}
                                    </TableCell>
                                ))}
                            </TableRow>
                        </TableHead>

                        <TableBody>
                            {product.map(data => (
                                <TableRow key={data.id}>
                                    <TableCell>{data.productImage}</TableCell>
                                    <TableCell>{data.sku}</TableCell>
                                    <TableCell>{data.productName}</TableCell>
                                    <TableCell>{data.subCategory.category.categoryName}</TableCell>
                                    <TableCell>{data.subCategory.subCategoryName}</TableCell>
                                    <TableCell>{data.weight}</TableCell>
                                    <TableCell>{data.purity}</TableCell>
                                    <TableCell>{data.sku}</TableCell>
                                    <TableCell>{data.metaltype}</TableCell>
                                    <TableCell>{data.price}</TableCell>
                                    <TableCell className='flex cursor'>
                                        <VisibilityOutlinedIcon />
                                        <EditOutlinedIcon onClick={()=>props.editProduct(data)}/>
                                        <DeleteOutlineOutlinedIcon onClick={()=>deleteProduct(data.id)} />
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </TableReuse>
                    <Paper className={classes.boxShadow}>
                        <TablePagination
                            rowsPerPageOptions={[10, 25, 100]}
                            component="div"
                            count={countData}
                            rowsPerPage={rowsPerPage}
                            page={page}
                            onChangePage={handleChangePage}
                            onChangeRowsPerPage={handleChangeRowsPerPage}
                        />
                    </Paper>
                    {open &&
                        <Delete
                            open={open}
                            clickConfirm={deleteConfirm}
                            confirmMsg="Are you sure you want to delete this product?"
                            handleClick={handleClick}
                        />
                    }
                </div>
            }
        </>
    )
}
