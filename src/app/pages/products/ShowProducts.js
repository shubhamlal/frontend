import React, { useState } from 'react'
import ShowProductTable from './ShowProductTable'
import { Dialog, DialogTitle } from '@material-ui/core'
import ClearRoundedIcon from '@material-ui/icons/ClearRounded';
import EditProductForm from './EditProductForm';

export default function ShowProducts() {
    const [editData,setEditData]=useState()
    const [open,setOpen]=useState(false)
    const [updateData,setUpdateData]=useState()

    const editProduct=(data)=>{
        setEditData(data)
        setOpen(!open)
    }

    const handleClick =()=>{
        setOpen(!open)
    }
    console.log(editData)
    return (
        <div>
            <ShowProductTable
                editProduct={(data)=>editProduct(data)}
                updateData={updateData}
            />

            <Dialog
                fullWidth={true}
                maxWidth='sm'
                open={open}
                onClose={handleClick}
                aria-labelledby="max-width-dialog-title"
                className='dailogMain'
            >
                <div className="closeIcon"  onClick={handleClick}>
                    <ClearRoundedIcon/>
                </div>
                <DialogTitle id="max-width-dialog-title">Update Product</DialogTitle>
                <EditProductForm
                    editData={editData}
                    open={()=>setOpen(false)}
                    updateData={(updateProduct)=>setUpdateData(updateProduct)}
                />
            </Dialog>
        </div>
    )
}
