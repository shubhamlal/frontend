import React, { useState, useEffect } from 'react'
import { Formik } from 'formik'
import { TextField, MenuItem } from '@material-ui/core'
import ErrorValidation from '../errors/ErrorValidation'
import { BASE_URL, SUBCATEGORY, PRODUCTS } from '../../API/APIEndpoints'
import APIServices from '../../API/APIServices'
import * as Yup from 'yup'
import clsx from "clsx";

export default function EditProductForm(props) {
    const [subCategory,setSubCategory]=useState([])
    const [loadingButtonStyle, setLoadingButtonStyle] = useState({
        paddingRight: "2.5rem"
      });
    const [loading, setLoading] = useState(false);

    const enableLoading = () => {
        setLoading(true);
        setLoadingButtonStyle({ paddingRight: "3.5rem" });
      };
    
    const disableLoading = () => {
        setLoading(false);
        setLoadingButtonStyle({ paddingRight: "2.5rem" });
    };

    async function fetchAPI(){
        const url=BASE_URL+SUBCATEGORY
        const fetchData=await new APIServices().get(url)
        const data = fetchData.results.data
        setSubCategory(data)
    }

    useEffect(()=>{
        fetchAPI()
    },[])
    console.log(subCategory)

    const validationSchema = Yup.object().shape({
        subCategoryId:Yup.string()
        .required("Sub Category is Required"),

        sku:Yup.string()
        .required("SKU Code is Required"),

        productName:Yup.string()
        .required("Product Name is Required"),

        height:Yup.number()
        .required("Weight is Required")
        .positive("enter only positive number"),

        price:Yup.number()
        .required("Price is Required")
        .positive('enter only positive'),

        purity:Yup.number()
            .required('Purity is Required')
            .positive('enter only positive number'),

        metaltype:Yup.string()
            .required('Jewellery Type is Required')
    })
    return (
        <Formik
            initialValues={{
                subCategoryId:props.editData.subCategoryId,
                sku:props.editData.sku,
                productName:props.editData.productName,
                height:props.editData.height,
                price:props.editData.price,
                purity:props.editData.purity,
                metaltype:props.editData.metaltype,
                id:props.editData.id
            }}

            validationSchema={validationSchema}

            onSubmit={(values,{setSubmitting,resetForm})=>{
                enableLoading()
                setSubmitting(true)

                setTimeout(async()=>{
                    const url= BASE_URL+ PRODUCTS + '/'+values.id
                    const updateProductData ={
                        subCategoryId:values.subCategoryId,
                        sku:props.editData.sku,
                        productName:values.productName,
                        height:values.height,
                        price:values.price,
                        purity:values.purity,
                        metaltype:values.metaltype
                    }

                    const updateProduct = await new APIServices().put(url,updateProductData)
                    disableLoading()
                    props.open()
                    props.updateData(updateProduct)
                    console.log(updateProduct)
                },1000)
            }}

        >
            {({values,errors,touched,handleChange,handleBlur,handleSubmit,isSubmitting})=>(
                <form
                    autoComplete="off"
                    className="kt-form"
                    onSubmit={handleSubmit}
                >
                    <div className="form-group">
                        <TextField
                            select
                            label="Select SubCategory"
                            value={values.subCategoryId}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            helperText="select category"
                            id="subCategoryId"
                            name="subCategoryId"
                            className="subCategoryDrop"
                            >
                            {subCategory.map((data) => (
                                <MenuItem key={data.id} value={data.id}>
                                    {data.subCategoryName}
                                </MenuItem>
                            ))}
                        </TextField>
                    </div>
                    <ErrorValidation touched={touched.subCategoryId} message={errors.subCategoryId} />
                    

                    <div className="form-group">
                        <TextField 
                            // type="subCategoryName"
                            label="SKU Code"
                            margin="normal"
                            className={touched.sku && errors.sku ? "has-error kt-width-full":"kt-width-full"}
                            name="sku"
                            onBlur={handleBlur}
                            onChange={handleChange}
                            value={values.sku}
                        />
                    </div>
                    <ErrorValidation touched={touched.sku} message={errors.sku} />

                    <div className="form-group">
                        <TextField 
                            // type="subCategoryName"
                            label="Product Name"
                            margin="normal"
                            className={touched.productName && errors.productName ? "has-error kt-width-full":"kt-width-full"}
                            name="productName"
                            onBlur={handleBlur}
                            onChange={handleChange}
                            value={values.productName}
                        />
                    </div>
                    <ErrorValidation touched={touched.productName} message={errors.productName} />

                    <div className="form-group">
                        <TextField 
                            // type="subCategoryName"
                            type="number"
                            label="Weight"
                            margin="normal"
                            className={touched.height && errors.height ? "has-error kt-width-full":"kt-width-full"}
                            name="height"
                            onBlur={handleBlur}
                            onChange={handleChange}
                            value={values.height}
                        />
                    </div>
                    <ErrorValidation touched={touched.height} message={errors.height} />

                    <div className="form-group">
                        <TextField 
                            // type="subCategoryName"
                            type="number"
                            label="Price"
                            margin="normal"
                            className={touched.price && errors.price ? "has-error kt-width-full":"kt-width-full"}
                            name="price"
                            onBlur={handleBlur}
                            onChange={handleChange}
                            value={values.price}
                        />
                    </div>
                    <ErrorValidation touched={touched.price} message={errors.price} />

                    <div className="form-group">
                        <TextField 
                            // type="subCategoryName"
                            type="number"
                            label="Purity"
                            margin="normal"
                            className={touched.purity && errors.purity ? "has-error kt-width-full":"kt-width-full"}
                            name="purity"
                            onBlur={handleBlur}
                            onChange={handleChange}
                            value={values.purity}
                        />
                    </div>
                    <ErrorValidation touched={touched.purity} message={errors.purity} />

                    <div className="form-group">
                        <TextField 
                            // type="subCategoryName"
                            label="Jewellery Type"
                            margin="normal"
                            className={touched.metaltype && errors.metaltype ? "has-error kt-width-full":"kt-width-full"}
                            name="metaltype"
                            onBlur={handleBlur}
                            onChange={handleChange}
                            value={values.metaltype}
                        />
                    </div>
                    <ErrorValidation touched={touched.metaltype} message={errors.metaltype} />

                    <button 
                        id="kt_login_signin_submit"
                        type="submit"
                        disabled={isSubmitting}
                        className={`btn btn-primary btn-elevate kt-login__btn-primary ${clsx(
                            {
                                "kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light": loading
                            }
                        )}`}
                        style={loadingButtonStyle}>Update</button>
                </form>
            )}
        </Formik>
    )
}
