import React, { useState } from 'react'
import SubCategoryForm from './SubCategoryForm'
import { Dialog, DialogTitle, Button } from '@material-ui/core'
import ClearRoundedIcon from '@material-ui/icons/ClearRounded';
import SubCategoryTable from './SubCategoryTable';

export default function SubCategory() {
    const [edit,setEdit]=useState(false)
    const [open,setOpen]=useState(false)
    const [add,setAdd]=useState()
    const [editData,setEditData]=useState()
    const [updateData,setUpdateData]=useState()

    const handleClick =()=>{
        setOpen(!open)
        setEdit(false)
    }

    const editSubCategory=(data)=>{
        setEditData(data)
        setOpen(!open)
        setEdit(true)
    }

    console.log(editData)
    return (
        <div>
            <div className="textRight">
                <Button onClick={handleClick} color="primary" variant="contained">Create Sub-Category</Button>
            </div>

            <SubCategoryTable
                add={add}
                updateData={updateData}
                editSubCategory={(data)=>editSubCategory(data)}
            />

            <Dialog
                fullWidth={true}
                maxWidth='sm'
                open={open}
                onClose={handleClick}
                aria-labelledby="max-width-dialog-title"
                className='dailogMain'
            >
                <div className="closeIcon"  onClick={handleClick}>
                    <ClearRoundedIcon/>
                </div>
                <DialogTitle id="max-width-dialog-title">{edit ? 'Update Sub-Category':'Create Sub-Category'}</DialogTitle>
                <SubCategoryForm 
                    open={()=>setOpen(false)}
                    edit={edit}
                    editData={editData}
                    addData={(subCategoryPost)=>setAdd(subCategoryPost)}
                    updateData={(updateSubCategory)=>setUpdateData(updateSubCategory)}
                />
            </Dialog>
        </div>
    )
}
