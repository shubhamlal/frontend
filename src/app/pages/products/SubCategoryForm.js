import React, { useEffect, useState } from 'react'
import { Formik } from 'formik'
import { TextField, MenuItem, DialogContent } from '@material-ui/core'
import { BASE_URL,CATEGORY, SUBCATEGORY } from '../../API/APIEndpoints'
import APIServices from '../../API/APIServices'
import clsx from "clsx";
import * as Yup from 'yup'
import ErrorValidation from '../errors/ErrorValidation'

export default function SubCategoryForm(props) {
    const[category,setCategory]= useState([])
    // const [open,setOpen]=useState(props.open)
    const [loadingButtonStyle, setLoadingButtonStyle] = useState({
        paddingRight: "2.5rem"
      });
    const [loading, setLoading] = useState(false);
    
    async function fetchCategory(){
        const url = BASE_URL + CATEGORY
        const categoryMainData = await new APIServices().get(url)
        const categoryData = categoryMainData.results.data
        setCategory(categoryData)
    }
    useEffect(()=>{
        fetchCategory()
    },[])


      
    const enableLoading = () => {
        setLoading(true);
        setLoadingButtonStyle({ paddingRight: "3.5rem" });
      };
    
    const disableLoading = () => {
        setLoading(false);
        setLoadingButtonStyle({ paddingRight: "2.5rem" });
    };


    const validationSchema = Yup.object().shape({
        subCategoryName:Yup.string()
        .required("Category Name is Required"),

        categoryId:Yup.number()
        .required("Category is Required")
    })
    console.log(category)
    return (
        <DialogContent>
            <Formik
                initialValues={{
                    subCategoryName:props.edit ? props.editData.subCategoryName:'',
                    categoryId:props.edit ? props.editData.categoryId:'',
                    id:props.edit ? props.editData.id:''
                }}

                validationSchema={validationSchema}

                onSubmit={(values, {setSubmitting,resetForm }) => {
                    enableLoading();
                    setSubmitting(true)

                    if(props.edit){
                        setTimeout(async()=>{
                            const url = BASE_URL + SUBCATEGORY + '/' + values.id
                            const updateSubCategoryData ={
                                subCategoryName:values.subCategoryName,
                                categoryId:values.categoryId
                            } 
                            const updateSubCategory = await new APIServices().put(url,updateSubCategoryData)
                            if(updateSubCategory.error){
                                console.log('error')
                                setSubmitting(false)
                                disableLoading()
                            }else{
                                disableLoading()
                                resetForm()
                                props.open()
                                props.updateData(updateSubCategory)
                            }
                        })
                    }else{
                        setTimeout(async()=>{
                        const url = BASE_URL+SUBCATEGORY
                        const subCategory = {
                            subCategoryName:values.subCategoryName,
                            categoryId:values.categoryId
                        }
                            const subCategoryPost = await new APIServices().post(url,subCategory)

                            if(subCategoryPost.error){
                                console.log('error')
                                setSubmitting(false)
                                disableLoading()
                            }else{
                                console.log(subCategoryPost)
                                disableLoading()
                                resetForm()
                                props.open()
                                props.addData(subCategoryPost)
                            }
                        },1000)
                    }
                }}
            >
                {({values,errors,touched,handleChange,handleBlur,handleSubmit,isSubmitting})=>(
                    <form
                        autoComplete="off"
                        className="kt-form"
                        onSubmit={handleSubmit}
                    >
                        <div className="form-group">
                            <TextField
                                type="subCategoryName"
                                label="Sub-Category Name"
                                margin="normal"
                                className={touched.subCategoryName && errors.subCategoryName ? "has-error kt-width-full":"kt-width-full"}
                                name="subCategoryName"
                                onBlur={handleBlur}
                                onChange={handleChange}
                                value={values.subCategoryName}
                            />
                        </div>
                        <ErrorValidation touched={touched.subCategoryName} message={errors.subCategoryName} />

                        <div className="form-group">
                        <TextField
                            select
                            label="Select"
                            value={values.categoryId}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            helperText="select category"
                            id="categoryId"
                            name="categoryId"
                            className="subCategoryDrop"
                            >
                            {category.map((category) => (
                                <MenuItem key={category.id} value={category.id}>
                                {category.categoryName}
                                </MenuItem>
                            ))}
                            </TextField>
                        </div>
                        <ErrorValidation touched={touched.categoryId} message={errors.categoryId} />


                        <button 
                            id="kt_login_signin_submit"
                            type="submit"
                            disabled={isSubmitting}
                            className={`btn btn-primary btn-elevate kt-login__btn-primary ${clsx(
                                {
                                    "kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light": loading
                                }
                            )}`}
                            style={loadingButtonStyle}>{props.edit ? 'Update':'Add'}</button>
                    </form>
                )}
            </Formik>
        </DialogContent>
    )
}
