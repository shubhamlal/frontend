import React, { useEffect, useState } from 'react'
import { BASE_URL, BULKUPLOADFILE, GETBULKREPORT } from '../../API/APIEndpoints'
import APIServices from '../../API/APIServices'
import TableReuse from '../home/table/TableReuse'
import { TableHead, TableRow, TableCell, TableBody, Button } from '@material-ui/core'

export default function BulkUploadReport() {
    const [reportFile,setReportFile]=useState([])
    const [columns]=useState([
        {id:1,name:'File Name'},
        {id:2,name:'Username'},
        {id:3,name:'Time'},
        {id:4,name:'Status'},
        {id:5,name:'Action'}
    ])
    async function fetchAPI(){
        const url = BASE_URL+BULKUPLOADFILE
        const reportData = await new APIServices().get(url)
        const reports = reportData.results.data
        setReportFile(reports)
        console.log(reports)
    }

    useEffect(()=>{
        fetchAPI()
    },[])

    const download=(data)=>{
        let url =data.url;
        let a = document.createElement('a');
        a.href = url;
        a.download = 'employees.csv';
        a.click();
    }

    const fileDownload=(url)=>{
        const link = document.createElement('a');
        link.href = url;
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link)
    }

    const reportDownload=async(id)=>{
        const url=BASE_URL+BULKUPLOADFILE+GETBULKREPORT+`/by-file-id?fileId=${id}`
        const download = await new APIServices().get(url)
        console.log(download)
        console.log(url)
    }   
    return (
        <>
            {reportFile.length>0 &&
                <div>
                    <TableReuse>
                        <TableHead>
                            <TableRow>
                                {columns.map(data =>(
                                    <TableCell key={data.id}>
                                        {data.name}
                                    </TableCell>
                                ))}
                            </TableRow>
                        </TableHead>

                        <TableBody>
                            {reportFile.map(data =>(
                                <TableRow key={data.id}>
                                    <TableCell>{data.originalname}</TableCell>
                                    <TableCell>{data.user.first_name}</TableCell>
                                    <TableCell>{data.createdAt}</TableCell>
                                    <TableCell>{data.status}</TableCell>
                                    <TableCell className="flex">
                                        <Button variant="contained" color='primary' className='marginRight' onClick={()=>fileDownload(data.URL)}>File</Button>
                                        <Button variant="contained" color='primary' onClick={()=>reportDownload(data.id)}>Report</Button>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </TableReuse>
                </div>
            }
        </>
    )
}
