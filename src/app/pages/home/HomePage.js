import React, { Suspense, lazy } from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import Builder from "./Builder";
import Dashboard from "./Dashboard";
import DocsPage from "./docs/DocsPage";
import { LayoutSplashScreen } from "../../../_metronic";
import Test from "../Test";
import BulkUploadProducts from "../products/BulkUploadProducts";
import BulkUploadDesign from "../products/BulkUploadDesign";
import ShowProducts from "../products/ShowProducts";
import MerchantDetails from "../userManagment/MerchantDetails";
import CreateMerchant from "../userManagment/CreateMerchant";
import WalletPrice from "./walletPrice/WalletPrice"
import UpdateWalletPrice from "./walletPrice/updateWalletPrice"
import TableTest from "./table/TableTest";
import BrokerDetails from "../userManagment/BrokerDetails"
import CreateBroker from "../userManagment/CreateBroker"
import CatalogPermission from "../userManagment/CatalogPermissions"
import CreateNewMerchant from "../userManagment/CreateNewMerchant";
import RolesPermission from "../userManagment/Roles&Permission"
import CreateNewRole from "../userManagment/CreateNewRole"
import OrderDetails from "../orderManagment/OrderDetails";
import Category from "../products/Category";
import EmailAlert from "./CSM/EmailAlert";
import SMSAlert from "./CSM/SMSAlert";
import EmailForm from "./CSM/EmailForm";
import UpdateEmailAlerts from "./CSM/UpdateEmailAlerts";
import SMSForm from "./CSM/SMSForm";
import UpdateSMSAlerts from "./CSM/UpdateSMSAlerts"
import AdminLog from "./AdminLog/AdminLog"
import StoreId from "./StoreId/StoreId"
import StoreIdForm from "./StoreId/StoreIdForm"
import SubCategory from "../products/SubCategory";
import BulkUploadReport from "../products/BulkUploadReport";

const GoogleMaterialPage = lazy(() =>
  import("./google-material/GoogleMaterialPage")
);
const ReactBootstrapPage = lazy(() =>
  import("./react-bootstrap/ReactBootstrapPage")
);

export default function HomePage() {
  // useEffect(() => {
  //   console.log('Home page');
  // }, []) // [] - is required if you need only one call
  // https://reactjs.org/docs/hooks-reference.html#useeffect

  return (
    <Suspense fallback={<LayoutSplashScreen />}>
      <Switch>
        {
          /* Redirect from root URL to /dashboard. */
          <Redirect exact from="/" to="/dashboard" />
        }
        <Route path="/builder" component={Builder} />
        <Route path="/dashboard" component={Dashboard} />
        <Route path="/upload-product" component={BulkUploadProducts} />
        <Route path="/upload-design" component={BulkUploadDesign} />
        <Route path="/products" component={ShowProducts} />
        <Route path="/category" component={Category} />
        <Route path="/subCategory" component={SubCategory} />
        <Route path="/create-broker" component={CreateBroker}/>
        <Route path="/merchant-details" component={MerchantDetails}/>
        <Route path="/wallet-price" component={WalletPrice}/>
        <Route path="/update-wallet-price" component={UpdateWalletPrice}/>
        <Route path="/email-alert" component={EmailAlert}/>
        <Route path="/sms-alert" component={SMSAlert}/>
        <Route path="/email-form" component={EmailForm}/>
        <Route path="/update-email-alerts" component={UpdateEmailAlerts}/>
        <Route path="/sms-form" component={SMSForm}/>
        <Route path="/update-sms-alerts" component={UpdateSMSAlerts}/>
        <Route path="/admin-log" component={AdminLog}/>
        <Route path="/create-vle-id" component={StoreId}/>
        <Route path="/store-id-form" component={StoreIdForm}/>
        <Route path="/create-merchant" component={CreateMerchant} />
        <Route path="/broker-details" component={BrokerDetails}/>
        <Route path="/catalog-permission" component={CatalogPermission}/>
        <Route path="/create-new-merchant" component={CreateNewMerchant}/>
        {/* <Route path="/roles-permission" component={RolesPermission}/> */}
        {/* <Route path="/create-new-role" component={CreateNewRole}/> */}
        <Route path="/order-details" component={OrderDetails} />
        <Route path="/bulkUploadReport" component={BulkUploadReport} />
        <Route path="/test" component={Test} />
        <Redirect to="/error/error-v1" />
      </Switch>
    </Suspense>
  );
}
