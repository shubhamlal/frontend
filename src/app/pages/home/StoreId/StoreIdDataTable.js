import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import DeleteOutlineOutlinedIcon from '@material-ui/icons/DeleteOutlineOutlined';
import { Paper , Table, TableHead, TableRow, TableBody, TableCell, Container, TablePagination} from '@material-ui/core';
import TableReuse from '../table/TableReuse'

const useStyles = makeStyles({
    root: {
      width: '100%',
    },
    container: {
      maxHeight: 440,
      overflowX:'auto',
      padding:0
    },
    boxShadow:{
        boxShadow:"none !important"
    }
  });

  const columns =[
      {id:'srno',label:'Sr. No.'},
      {id:'id',label:'ID'},
      {id:'vleId',label:'VLE ID'},
      {id:'delete',label:'delete'}
  ]
export default function StoreId() {
    const classes = useStyles();
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);

      const handleChangePage = (event, newPage) => {
        setPage(newPage);
      };
    
      const handleChangeRowsPerPage = event => {
        setRowsPerPage(+event.target.value);
        setPage(0);
      };

      const rows =[
         {srno:1,id:30,vleId:'SDR46556'},
         {srno:2,id:30,vleId:'SDR46556'},
         {srno:3,id:30,vleId:'SDR46556'},
         {srno:4,id:30,vleId:'SDR46556'},
         {srno:5,id:30,vleId:'SDR46556'},
         {srno:6,id:30,vleId:'SDR46556'},
         {srno:7,id:30,vleId:'SDR46556'},
         {srno:8,id:30,vleId:'SDR46556'}
      ]
    return (
        <div>
            <TableReuse>
                <TableHead>
                    <TableRow>
                        {columns.map(column => (
                            <TableCell 
                                key={column.id}
                            >
                                {column.label}
                            </TableCell>
                        ))}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.slice(page * rowsPerPage,page * rowsPerPage + rowsPerPage).map((row,i) =>{
                        return(
                            <TableRow hover role="checkbox" key={i} tabIndex={-1}>
                                <TableCell>{row.srno}</TableCell>
                                <TableCell>{row.id}</TableCell>
                                <TableCell>{row.vleId}</TableCell>
                                <TableCell><DeleteOutlineOutlinedIcon /></TableCell>
                            </TableRow>
                        )
                    })}
                    </TableBody>
            </TableReuse>
            <Paper className={classes.boxShadow}>
                <TablePagination
                    rowsPerPageOptions={[10, 25, 100]}
                    component="div"
                    count={rows.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                />
            </Paper>
        </div>
    )
}

