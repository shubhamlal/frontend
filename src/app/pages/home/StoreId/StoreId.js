
import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import {Link} from 'react-router-dom';
import StoreIdDataTable from "./StoreIdDataTable"

export default function EmailAlert() {
    const useStyles = makeStyles(theme => ({
        button: {
          margin: theme.spacing(1),
          textAlign:'end'
        },
        input: {
          display: 'none',
          padding:'2% 0'
        },
      }));

      
      
    const classes = useStyles();
    return (
        <div className="bulkMain">
          <div style={{textAlign:'end'}}>
            <Link to="/store-id-form">
                <Button variant="contained" color="primary" className={classes.button}>
                  Create Store ID(Vle ID)
                </Button>
            </Link>
          </div>
          <StoreIdDataTable/>
        </div>
    )
}
