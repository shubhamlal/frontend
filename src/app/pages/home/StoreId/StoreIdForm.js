import React, { useState } from 'react'
import { Formik } from 'formik'
import * as Yup from 'yup'
import { TextField, FormControl, InputLabel, NativeSelect, Grid, Button,Backdrop,Fade,Modal } from '@material-ui/core'
import ErrorValidation from '../../errors/ErrorValidation'
import { makeStyles } from '@material-ui/core/styles';



const useStyles = makeStyles(theme => ({
    formControl: {
        margin: theme.spacing(1),
        width:'100%'
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        background:'rgba(0,0,0,0.6)'
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
        textAlign:'center',
        position:'relative'
      },
  }));

export default function CreateEmailAlert() {
    const classes = useStyles();

    const [alerts,setAlert]=useState()
    const [modal,setModal]=useState(false)
    const [error,setError]=useState(false)
    const [API,setAPI]=useState()
    console.log(API)
    console.log(alerts)
    console.log(error)

    const validationSchema = Yup.object().shape({
        vleId:Yup.string()
        .required("VLE ID cannot be blank"),


    });

 

 
    return (
        <div className="createUserForm">
            <h3>Create Store Id(VLE ID)</h3>

            <Formik
                initialValues={{
                  vleId:'',
                }}
                
                validationSchema={validationSchema}

                onSubmit={(values,{resetForm,setSubmitting})=>{
                    console.log(values)
                    console.log(error)
                    setSubmitting(true);
                    // if((API !==""||API!==undefined) && !error ){
                    const value={
                        vleId:values.vleId
                    }

                    setTimeout(()=>{
                        setAlert(value)
                        resetForm()
                        setAPI()
                        setSubmitting(false)
                    },1000)
               
                }}
            >
                {({values,errors,touched,handleChange,handleBlur,handleSubmit,isSubmitting})=>(
                    <form
                        autoComplete="off"
                        className="kt-form" 
                        onSubmit={handleSubmit}
                    >
                        <div>
                            <TextField
                                type="text"
                                label="VLE ID"
                                margin="normal"
                                className={touched.alerts && errors.alerts ? "has-error kt-width-full":"kt-width-full"}
                                name="vleId"
                                onBlur={handleBlur}
                                onChange={handleChange}
                                value={values.vleId}
                            />
                            <ErrorValidation touched={touched.vleId} message={errors.vleId} />
                        </div>
                       

                        <div style={{margin:'10px 0'}}>
                            <Button variant="contained" color="primary" type="submit">Create</Button>
                        </div>
                    </form> 
                )}

            </Formik>
        </div>
    )
}

