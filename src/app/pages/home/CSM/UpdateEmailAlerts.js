import React, { useState } from 'react'
import { Formik } from 'formik'
import * as Yup from 'yup'
import { TextField, FormControl, InputLabel, NativeSelect, Grid, Button,Backdrop,Fade,Modal } from '@material-ui/core'
import ErrorValidation from '../../errors/ErrorValidation'
import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles(theme => ({
    formControl: {
        margin: theme.spacing(1),
        width:'100%'
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        background:'rgba(0,0,0,0.6)'
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
        textAlign:'center',
        position:'relative'
      },
  }));

export default function UpdateEmailAlerts() {
    const classes = useStyles();

    const [alerts,setAlert]=useState()
    const [error,setError]=useState(false)
    

    const validationSchema = Yup.object().shape({
        alerts:Yup.string()
        .required("Alerts cannot be blank"),

        alertType:Yup.string().required("Alert Type cannot be blank"),

        alertsSubject:Yup.string().required("Alerts Subject cannot be blank"),

        alertContent:Yup.string().required("Alert Content cannot be blank"),

    });

 

 
    return (
        <div className="createUserForm">
            <h3>Update Email Alerts</h3>

            <Formik
                initialValues={{
                  alerts:'',
                alertType:'',
                alertsSubject:'',
                alertContent:''
                }}
                
                validationSchema={validationSchema}

                onSubmit={(values,{resetForm,setSubmitting})=>{
                    console.log(values)
                    console.log(error)
                    setSubmitting(true);
                    // if((API !==""||API!==undefined) && !error ){
                    const value={
                        alerts:value.alerts,
                        alertType:value.alertType,
                        alertsSubject:values.alertsSubject,
                        alertContent:values.alertContent,
                      
                    }

                    setTimeout(()=>{
                        setAlert(value)
                        resetForm()
                        setSubmitting(false)
                    },1000)
                // }else{
                //     setError(true)
                // }
                }}
            >
                {({values,errors,touched,handleChange,handleBlur,handleSubmit,isSubmitting})=>(
                    <form
                        autoComplete="off"
                        className="kt-form" 
                        onSubmit={handleSubmit}
                    >
                        <div>
                            <TextField
                                type="text"
                                label="Alerts"
                                margin="normal"
                                className={touched.alerts && errors.alerts ? "has-error kt-width-full":"kt-width-full"}
                                name="alerts"
                                onBlur={handleBlur}
                                onChange={handleChange}
                                value={values.alerts}
                            />
                            <ErrorValidation touched={touched.alerts} message={errors.alerts} />
                        </div>
                        <div>
                            <TextField
                                type="text"
                                label="Alert Type"
                                margin="normal"
                                className={touched.name && errors.name ? "has-error kt-width-full":"kt-width-full"}
                                name="alertType"
                                onBlur={handleBlur}
                                onChange={handleChange}
                                value={values.alertType}
                            />
                            <ErrorValidation touched={touched.alertType} message={errors.alertType} />
                        </div>
                        <div>
                            <TextField
                                type="text"
                                label="Alerts Subject"
                                margin="normal"
                                className={touched.name && errors.name ? "has-error kt-width-full":"kt-width-full"}
                                name="alertsSubject"
                                onBlur={handleBlur}
                                onChange={handleChange}
                                value={values.alertsSubject}
                            />
                            <ErrorValidation touched={touched.alertsSubject} message={errors.alertsSubject} />
                        </div>
                        <div>
                            <TextField
                                type="text"
                                label="Alert Content"
                                margin="normal"
                                className={touched.name && errors.name ? "has-error kt-width-full":"kt-width-full"}
                                name="alertContent"
                                onBlur={handleBlur}
                                onChange={handleChange}
                                value={values.alertContent}
                            />
                            <ErrorValidation touched={touched.alertContent} message={errors.alertContent} />
                        </div>

                        <div style={{margin:'10px 0'}}>
                            <Button variant="contained" color="primary" type="submit">Save</Button>
                        </div>
                    </form> 
                )}

            </Formik>
        </div>
    )
}

