import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import {Link} from 'react-router-dom'
import SMSDataTable from './SMSDataTable';

export default function EmailAlert() {
    const useStyles = makeStyles(theme => ({
        button: {
          margin: theme.spacing(1),
          textAlign:'end'
        },
        input: {
          display: 'none',
          padding:'2% 0'
        },
      }));

      
      
    const classes = useStyles();
    return (
        <div className="bulkMain">
          <div style={{textAlign:'end'}}>
            <Link to="/sms-form">
                <Button variant="contained" color="primary" className={classes.button}>
                  Create SMS Alerts
                </Button>
            </Link>
          </div>
          <SMSDataTable/>
        </div>
    )
}
