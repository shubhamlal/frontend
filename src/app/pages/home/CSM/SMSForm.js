import React, { useState } from 'react'
import { Formik } from 'formik'
import * as Yup from 'yup'
import { TextField, FormControl, InputLabel, NativeSelect, Grid, Button,Backdrop,Fade,Modal } from '@material-ui/core'
import ErrorValidation from '../../errors/ErrorValidation'
import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles(theme => ({
    formControl: {
        margin: theme.spacing(1),
        width:'100%'
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        background:'rgba(0,0,0,0.6)'
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
        textAlign:'center',
        position:'relative'
      },
  }));

export default function CreateEmailAlert() {
    const classes = useStyles();

    const [alerts,setAlert]=useState()
    const [error,setError]=useState(false)
    const [API,setAPI]=useState()
    console.log(API)
    console.log(alerts)
    console.log(error)

    const validationSchema = Yup.object().shape({
        alertFor:Yup.string()
        .required("Alerts For  cannot be blank"),

        text:Yup.string().required("Text cannot be blank"),
        alertStatus:Yup.string().required("Alert Status is required"),

          });

 

 
    return (
        <div className="createUserForm">
            <h3>Create SMS Alert</h3>

            <Formik
                initialValues={{
                  alertFor:'',
                    text:'',
                    alertStatus:''
                }}
                
                validationSchema={validationSchema}

                onSubmit={(values,{resetForm,setSubmitting})=>{
                    console.log(values)
                    console.log(error)
                    setSubmitting(true);
                    // if((API !==""||API!==undefined) && !error ){
                    const value={
                        alertFor:value.alertFor,
                        text:value.text,
                        alertStatus:value.alertStatus
                       
                    }

                    setTimeout(()=>{
                        setAlert(value)
                        resetForm()
                        setAPI()
                        setSubmitting(false)
                    },1000)
                // }else{
                //     setError(true)
                // }
                }}
            >
                {({values,errors,touched,handleChange,handleBlur,handleSubmit,isSubmitting})=>(
                    <form
                        autoComplete="off"
                        className="kt-form" 
                        onSubmit={handleSubmit}
                    >
                        <div>
                            <TextField
                                type="text"
                                label="Alert For"
                                margin="normal"
                                className={touched.alertFor && errors.alertFor ? "has-error kt-width-full":"kt-width-full"}
                                name="alertFor"
                                onBlur={handleBlur}
                                onChange={handleChange}
                                value={values.alertFor}
                            />
                            <ErrorValidation touched={touched.alertFor} message={errors.alertFor} />
                        </div>
                        <div>
                            <TextField
                                type="text"
                                label="Text"
                                margin="normal"
                                className={touched.name && errors.name ? "has-error kt-width-full":"kt-width-full"}
                                name="text"
                                onBlur={handleBlur}
                                onChange={handleChange}
                                value={values.text}
                            />
                            <ErrorValidation touched={touched.text} message={errors.text} />
                        </div>

                        <Grid item xs={6}>
                                <FormControl className={classes.formControl}>
                                    <InputLabel>Alert status</InputLabel>
                                    <NativeSelect
                                        name='alertStatus'
                                        value={values.alertStatus}
                                        onChange={handleChange}
                                        className={touched.alertStatus && errors.alertStatus ? "has-error kt-width-full":"kt-width-full"}
                                    >
                                        <option aria-label="None" value="" />
                                        <option value="InActive">In Active</option>
                                        <option value="Active">Active</option>
                                    </NativeSelect>
                                </FormControl>
                                <ErrorValidation touched={touched.alertStatus} message={errors.alertStatus} />
                            </Grid>
                        <div style={{margin:'10px 0'}}>
                            <Button variant="contained" color="primary" type="submit">Create</Button>
                        </div>

                    </form> 
                )}

            </Formik>
        </div>
    )
}

