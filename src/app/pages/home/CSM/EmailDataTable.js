import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import { Paper , Table, TableHead, TableRow, TableBody, TableCell, Container, TablePagination} from '@material-ui/core';
import TableReuse from '../table/TableReuse'
import { Link } from "react-router-dom";

const useStyles = makeStyles({
    root: {
      width: '100%',
    },
    container: {
      maxHeight: 440,
      overflowX:'auto',
      padding:0
    },
    boxShadow:{
        boxShadow:"none !important"
    }
  });

  const columns =[
      {id:'srno',label:'Sr. No.'},
      {id:'alertId',label:'Alert Id'},
      {id:'alerts',label:'Alerts'},
      {id:'alertType',label:'Alert Type'},
      {id:'alertsSubject',label:'Alert Subject'},
      {id:'edit',label:'edit'},
  ]
export default function EmailDataTable() {
    const classes = useStyles();
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);

      const handleChangePage = (event, newPage) => {
        setPage(newPage);
      };
    
      const handleChangeRowsPerPage = event => {
        setRowsPerPage(+event.target.value);
        setPage(0);
      };

      const rows =[
         {srno:1,alertId:4,alerts:"changed password",alertType:"email",alertsSubject:"itigershop change password"},
         {srno:2,alertId:3,alerts:"forgot password",alertType:"email",alertsSubject:"itigershop change password"},
         {srno:3,alertId:2,alerts:"Password Retrieved",alertType:"email",alertsSubject:"itigershop change password"},
         {srno:4,alertId:1,alerts:"Broker registration",alertType:"email",alertsSubject:"itigershop change password"}
      ]
    return (
        <div>
            <TableReuse>
                <TableHead>
                    <TableRow>
                        {columns.map(column => (
                            <TableCell 
                                key={column.id}
                            >
                                {column.label}
                            </TableCell>
                        ))}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.slice(page * rowsPerPage,page * rowsPerPage + rowsPerPage).map((row,i) =>{
                        return(
                            <TableRow hover role="checkbox" key={i} tabIndex={-1}>
                                <TableCell>{row.srno}</TableCell>
                                <TableCell>{row.alertId}</TableCell>
                                <TableCell>{row.alerts}</TableCell>
                                <TableCell>{row.alertType}</TableCell>
                                <TableCell>{row.alertsSubject}</TableCell>
                                <TableCell>
                                <Link
                    to="/update-email-alerts"
                    className="kt-link kt-login__link-forgot"
                  >
                    <EditOutlinedIcon/>
                  </Link>
                                </TableCell>
                            </TableRow>
                        )
                    })}
                    </TableBody>
            </TableReuse>
            <Paper className={classes.boxShadow}>
                <TablePagination
                    rowsPerPageOptions={[10, 25, 100]}
                    component="div"
                    count={rows.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                />
            </Paper>
        </div>
    )
}
