import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import { Paper , Table, TableHead, TableRow, TableBody, TableCell, Container, TablePagination} from '@material-ui/core';
import TableReuse from '../table/TableReuse'
import { Link } from "react-router-dom";

const useStyles = makeStyles({
    root: {
      width: '100%',
    },
    container: {
      maxHeight: 440,
      overflowX:'auto',
      padding:0
    },
    boxShadow:{
        boxShadow:"none !important"
    }
  });

  const columns =[
      {id:'srno',label:'Sr. No.'},
      {id:'alertFor',label:'Alert For'},
      {id:'edit',label:'edit'},
  ]
export default function SMSDataTable() {
    const classes = useStyles();
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);

      const handleChangePage = (event, newPage) => {
        setPage(newPage);
      };
    
      const handleChangeRowsPerPage = event => {
        setRowsPerPage(+event.target.value);
        setPage(0);
      };

      const rows =[
         {srno:1,alertFor:"Forgot Password"},
         {srno:2,alertFor:"Forgot Password"},
         {srno:3,alertFor:"Forgot Password"},
         {srno:4,alertFor:"Forgot Password"},
         {srno:5,alertFor:"Forgot Password"},
      ]
    return (
        <div>
            <TableReuse>
                <TableHead>
                    <TableRow>
                        {columns.map(column => (
                            <TableCell 
                                key={column.id}
                            >
                                {column.label}
                            </TableCell>
                        ))}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.slice(page * rowsPerPage,page * rowsPerPage + rowsPerPage).map((row,i) =>{
                        return(
                            <TableRow hover role="checkbox" key={i} tabIndex={-1}>
                                <TableCell>{row.srno}</TableCell>
                                <TableCell>{row.alertFor}</TableCell>
                                <TableCell>
                                <Link
                    to="/update-sms-alerts"
                    className="kt-link kt-login__link-forgot"
                  > 
                    <EditOutlinedIcon/>
                  </Link>
                                </TableCell>
                            </TableRow>
                        )
                    })}
                    </TableBody>
            </TableReuse>
            <Paper className={classes.boxShadow}>
                <TablePagination
                    rowsPerPageOptions={[10, 25, 100]}
                    component="div"
                    count={rows.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                />
            </Paper>
        </div>
    )
}
