
import React, { useState } from 'react'
import { Formik } from 'formik'
import * as Yup from 'yup'
import { TextField, FormControl, InputLabel, NativeSelect, Grid, Button,Backdrop,Fade,Modal } from '@material-ui/core'
import ErrorValidation from '../../errors/ErrorValidation'
import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles(theme => ({
    formControl: {
        margin: theme.spacing(1),
        width:'100%'
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        background:'rgba(0,0,0,0.6)'
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
        textAlign:'center',
        position:'relative'
      },
  }));

export default function UpdateWalletPriceDetails() {
    const classes = useStyles();

    const [alerts,setAlert]=useState()
    const [error,setError]=useState(false)
    

    const validationSchema = Yup.object().shape({
        walletPrice:Yup.string()
        .required("Wallet price cannot be blank"),

        walletGold:Yup.string().required("Wallet Gold price cannot be blank"),

        walletSilver:Yup.string().required("Wallet Silver Price cannot be blank"),

        forwardCost3Month:Yup.string().required("Forward Cost for 3 months value cannot be blank"),

        forwardCost6Month:Yup.string().required("Forward Cost for 6 months value cannot be blank"),

        forwardCost9Month:Yup.string().required("Forward Cost for 9 months value cannot be blank"),

        vatvalue:Yup.string().required("VAT Value cannot be blank"),

        exciseDuty:Yup.string().required("Excise Duty cannot be blank"),

        cancelValue:Yup.string().required("cancel Value cannot be blank"),



    });

 

 
    return (
        <div className="createUserForm">
            <h3>Update Wallet Price  Details</h3>

            <Formik
                initialValues={{
                    walletPrice:"",
                     walletGold:"",
                    walletSilver:"",
                forwardCost3Month:"",
                forwardCost6Month:"",
                forwardCost9Month:"",
                 vatvalue:"",
                exciseDuty:"",
                 cancelValue:""
                }}
                
                validationSchema={validationSchema}

                onSubmit={(values,{resetForm,setSubmitting})=>{
                    console.log(values)
                    console.log(error)
                    setSubmitting(true);
                    // if((API !==""||API!==undefined) && !error ){
                    const value={
                        walletPrice:values.walletPrice,
                        walletGold:values.walletGold,
                        walletSilver:values.walletSilver,
                        forwardCost3Month:values.forwardCost3Month,
                        forwardCost6Month:values.forwardCost6Month,
                        forwardCost9Month:values.forwardCost9Month,
                        vatvalue:values.vatvalue,
                        exciseDuty:values.exciseDuty,
                        cancelValue:values.cancelValue,
                    }

                    setTimeout(()=>{
                        setAlert(value)
                        resetForm()
                        setSubmitting(false)
                    },1000)
                // }else{
                //     setError(true)
                // }
                }}
            >
                {({values,errors,touched,handleChange,handleBlur,handleSubmit,isSubmitting})=>(
                    <form
                        autoComplete="off"
                        className="kt-form" 
                        onSubmit={handleSubmit}
                    >
                        <div>
                            <TextField
                                type="text"
                                label="Wallet Price"
                                margin="normal"
                                className={touched.walletPrice&& errors.walletPrice ? "has-error kt-width-full":"kt-width-full"}
                                name="walletPrice"
                                onBlur={handleBlur}
                                onChange={handleChange}
                                value={values.walletPrice}
                            />
                            <ErrorValidation touched={touched.walletPrice} message={errors.walletPrice} />
                        </div>
                        <div>
                            <TextField
                                type="text"
                                label="Wallet Gold Price(1 grms)"
                                margin="normal"
                                className={touched.name && errors.name ? "has-error kt-width-full":"kt-width-full"}
                                name="walletGold"
                                onBlur={handleBlur}
                                onChange={handleChange}
                                value={values.walletGold}
                            />
                            <ErrorValidation touched={touched.walletGold} message={errors.walletGold} />
                        </div>
                        <div>
                            <TextField
                                type="text"
                                label="Wallet Silver Price(1 grms)"
                                margin="normal"
                                className={touched.name && errors.name ? "has-error kt-width-full":"kt-width-full"}
                                name="walletSilver"
                                onBlur={handleBlur}
                                onChange={handleChange}
                                value={values.walletSilver}
                            />
                            <ErrorValidation touched={touched.walletSilver} message={errors.walletSilver} />
                        </div>
                        <div>
                            <TextField
                                type="text"
                                label="Forward cost for 3 Months Value"
                                margin="normal"
                                className={touched.name && errors.name ? "has-error kt-width-full":"kt-width-full"}
                                name="forwardCost3Month"
                                onBlur={handleBlur}
                                onChange={handleChange}
                                value={values.forwardCost3Month}
                            />
                            <ErrorValidation touched={touched.forwardCost3Month} message={errors.forwardCost3Month} />
                        </div>
                        <div>
                            <TextField
                                type="text"
                                label="Forward cost for 6 Months Value"
                                margin="normal"
                                className={touched.name && errors.name ? "has-error kt-width-full":"kt-width-full"}
                                name="forwardCost6Month"
                                onBlur={handleBlur}
                                onChange={handleChange}
                                value={values.forwardCost6Month}
                            />
                            <ErrorValidation touched={touched.forwardCost6Month} message={errors.forwardCost6Month} />
                        </div>

                        <div>
                            <TextField
                                type="text"
                                label="Forward cost for 9 Months Value"
                                margin="normal"
                                className={touched.name && errors.name ? "has-error kt-width-full":"kt-width-full"}
                                name="forwardCost9Month"
                                onBlur={handleBlur}
                                onChange={handleChange}
                                value={values.forwardCost9Month}
                            />
                            <ErrorValidation touched={touched.forwardCost9Month} message={errors.forwardCost9Month} />
                        </div>
                        <div>
                            <TextField
                                type="text"
                                label="VAT Value"
                                margin="normal"
                                className={touched.name && errors.name ? "has-error kt-width-full":"kt-width-full"}
                                name="vatvalue"
                                onBlur={handleBlur}
                                onChange={handleChange}
                                value={values.vatvalue}
                            />
                            <ErrorValidation touched={touched.vatvalue} message={errors.vatvalue} />
                        </div>
                        <div>
                            <TextField
                                type="text"
                                label="Excise Duty"
                                margin="normal"
                                className={touched.name && errors.name ? "has-error kt-width-full":"kt-width-full"}
                                name="exciseDuty"
                                onBlur={handleBlur}
                                onChange={handleChange}
                                value={values.exciseDuty}
                            />
                            <ErrorValidation touched={touched.exciseDuty} message={errors.exciseDuty} />
                        </div>
                        <div>
                            <TextField
                                type="text"
                                label="Cancle Value"
                                margin="normal"
                                className={touched.name && errors.name ? "has-error kt-width-full":"kt-width-full"}
                                name="cancelValue"
                                onBlur={handleBlur}
                                onChange={handleChange}
                                value={values.cancelValue}
                            />
                            <ErrorValidation touched={touched.cancelValue} message={errors.cancelValue} />
                        </div>


                        <div style={{margin:'10px 0'}}>
                            <Button variant="contained" color="primary" type="submit">Save</Button>
                        </div>
                    </form> 
                )}

            </Formik>
        </div>
    )
}

