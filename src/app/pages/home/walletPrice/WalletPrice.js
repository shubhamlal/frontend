import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import DeleteOutlineOutlinedIcon from '@material-ui/icons/DeleteOutlineOutlined';
import { Paper , Table, TableHead, TableRow, TableBody, TableCell, Container, TablePagination} from '@material-ui/core';
import TableReuse from '../table/TableReuse'
import { Link } from "react-router-dom";

const useStyles = makeStyles({
    root: {
      width: '100%',
    },
    container: {
      maxHeight: 440,
      overflowX:'auto',
      padding:0
    },
    boxShadow:{
        boxShadow:"none !important"
    }
  });

  const columns =[
      {id:'srno',label:'Sr. No.'},
      {id:'walletPrice',label:'Wallet Price'},
      {id:'walletGold',label:'Wallet Gold Price(1 grms)'},
      {id:'walletSilver',label:'wallet Silver Price(1 grms)'},
      {id:'costForMonth',label:'Forward Cost For 3 Month value(%)'},
      {id:'costFor6Month',label:'Forward Cost For 6 Month value(%)'},
      {id:'costFor9Month',label:'Forward Cost For 9 Month value(%)'},
      {id:'vat',label:'VAT Value(%)'},
      {id:'excise',label:'Excise Duty Value(%)'},
      {id:'cancel',label:'Cancel Value(%)'},
      {id:'edit',label:'edit'},
  ]
export default function ShowProducts() {
    const classes = useStyles();
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);

      const handleChangePage = (event, newPage) => {
        setPage(newPage);
      };
    
      const handleChangeRowsPerPage = event => {
        setRowsPerPage(+event.target.value);
        setPage(0);
      };

      const rows =[
         {srno:1,walletPrice:3,walletGold:3240.0000,walletSilver:37.001,costForMonth:1.5,costFor6Month:3.5,costFor9Month:5.5,vat:3.0,excise:0.0,cancel:2.4},
        
      ]
    return (
        <div>
            <TableReuse>
                <TableHead>
                    <TableRow>
                        {columns.map(column => (
                            <TableCell 
                                key={column.id}
                            >
                                {column.label}
                            </TableCell>
                        ))}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.slice(page * rowsPerPage,page * rowsPerPage + rowsPerPage).map((row,i) =>{
                        return(
                            <TableRow hover role="checkbox" key={i} tabIndex={-1}>
                                <TableCell>{row.srno}</TableCell>
                                <TableCell>{row.walletPrice}</TableCell>
                                <TableCell>{row.walletGold}</TableCell>
                                <TableCell>{row.walletSilver}</TableCell>
                                <TableCell>{row.costForMonth}</TableCell>
                                <TableCell>{row.costFor6Month}</TableCell>
                                <TableCell>{row.costFor9Month}</TableCell>
                                <TableCell>{row.vat}</TableCell>
                                <TableCell>{row.excise}</TableCell>
                                <TableCell>{row.cancel}</TableCell>
                                <TableCell>
                                <Link
                    to="/update-wallet-price"
                    className="kt-link kt-login__link-forgot"
                  >
                    {/* forgot password */}
                    <EditOutlinedIcon/>
                  </Link>
                                </TableCell>
                            </TableRow>
                        )
                    })}
                    </TableBody>
            </TableReuse>
            <Paper className={classes.boxShadow}>
                <TablePagination
                    rowsPerPageOptions={[10, 25, 100]}
                    component="div"
                    count={rows.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                />
            </Paper>
        </div>
    )
}
