import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import { Paper , Table, TableHead, TableRow, TableBody, TableCell, Container, TablePagination} from '@material-ui/core';
import TableReuse from '../table/TableReuse'


const useStyles = makeStyles({
    root: {
      width: '100%',
    },
    container: {
      maxHeight: 440,
      overflowX:'auto',
      padding:0
    },
    boxShadow:{
        boxShadow:"none !important"
    }
  });

  const columns =[
      {id:'adminId',label:'Admin ID'},
      {id:'emailId',label:'Email ID'},
      {id:'date',label:'Date'},
      {id:'action',label:'Action'},
  ]
export default function AdminLog() {
    const classes = useStyles();
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);

      const handleChangePage = (event, newPage) => {
        setPage(newPage);
      };
    
      const handleChangeRowsPerPage = event => {
        setRowsPerPage(+event.target.value);
        setPage(0);
      };

      const rows =[
         {adminId:6,emailId:"demo12@gmail.com",date:"2019-2-3",action:"created new stored id"},
         {adminId:6,emailId:"demo12@gmail.com",date:"2019-2-3",action:"created new stored id"},
         {adminId:6,emailId:"demo12@gmail.com",date:"2019-2-3",action:"created new stored id"},
         {adminId:6,emailId:"demo12@gmail.com",date:"2019-2-3",action:"created new stored id"},
         {adminId:6,emailId:"demo12@gmail.com",date:"2019-2-3",action:"created new stored id"},
      ]
    return (
        <div>
          
            <TableReuse>
                <TableHead>
                    <TableRow>
                        {columns.map(column => (
                            <TableCell 
                                key={column.id}
                            >
                                {column.label}
                            </TableCell>
                        ))}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.slice(page * rowsPerPage,page * rowsPerPage + rowsPerPage).map((row,i) =>{
                        return(
                            <TableRow hover role="checkbox" key={i} tabIndex={-1}>
                                <TableCell>{row.adminId}</TableCell>
                                <TableCell>{row.emailId}</TableCell>
                                <TableCell>{row.date}</TableCell>
                                <TableCell>{row.action}</TableCell>
                            </TableRow>
                        )
                    })}
                    </TableBody>
            </TableReuse>
            <Paper className={classes.boxShadow}>
                <TablePagination
                    rowsPerPageOptions={[10, 25, 100]}
                    component="div"
                    count={rows.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                />
            </Paper>
        </div>
    )
}
