import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import { Paper , Table, Container} from '@material-ui/core';

const useStyles = makeStyles({
    root: {
      width: '100%',
    },
    container: {
      maxHeight: 440,
      overflowX:'auto',
      padding:0
    },
  });
export default function TableReuse(props) {
    const classes = useStyles();
    return (
        <Paper className={classes.root}>
            <Container className={classes.container}>
                <Table stickyHeader aria-label="sticky table">
                    {props.children}
                </Table>
            </Container>
        </Paper>
    )
}
