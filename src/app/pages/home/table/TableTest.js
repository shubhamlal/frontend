import React,{useState, useRef, useEffect} from 'react'
import { makeStyles } from '@material-ui/core/styles';
import TableReuse from './TableReuse'
import { TableRow, TableCell, TableHead, TableBody, TablePagination, Paper, TextField } from '@material-ui/core'
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import DeleteOutlineOutlinedIcon from '@material-ui/icons/DeleteOutlineOutlined';
import axios from 'axios';

const useStyles = makeStyles({
    boxShadow:{
        boxShadow:"none !important"
    }
  });

export default function TableTest() {
    const classes = useStyles();
    const [page, setPage] = React.useState(0);
    const[isfilter,setIsfilter]=useState(false)
    const [rowsPerPage, setRowsPerPage] = React.useState(10);
    const [searchD,setSearchD]=useState()
    const [state,setState] = React.useState({})
    const [selectedState,setSelectedState]=useState()
    const [demo,setDemo]=useState([])
    const [rows,setRows]=useState([
        {partnerName:'partner name',masterCommission:'Gold Coin (5%)',categoryCommission:'Gold Jewellery (19%)',APIKey:'123456',status:'active'},
        {partnerName:'partner name two',masterCommission:'Gold jewellery (5%)',categoryCommission:'Gold Jewellery (19%)',APIKey:'12345d26',status:'active'},
        {partnerName:'partner name three',masterCommission:'Gold jewellery (5%)',categoryCommission:'Gold Jewellery (19%)',APIKey:'12345d26',status:'active'},
        {partnerName:'partner name four',masterCommission:'Gold Coin (5%)',categoryCommission:'Gold Jewellery (19%)',APIKey:'12345d26',status:'active'},
        {partnerName:'partner name five',masterCommission:'Gold Coin (10%)',categoryCommission:'Gold Jewellery (19%)',APIKey:'12345d26',status:'active'},
        {partnerName:'partner name five',masterCommission:'Gold Coin (10%)',categoryCommission:'Gold Jewellery (19%)',APIKey:'12345d26',status:'inactive'},
        {partnerName:'partner name five',masterCommission:'Gold Coin (5%)',categoryCommission:'Gold Jewellery (19%)',APIKey:'12345d26',status:'active'},
        {partnerName:'partner name five',masterCommission:'Gold Coin (5%)',categoryCommission:'Gold Jewellery (19%)',APIKey:'12345d26',status:'active'},
        {partnerName:'partner name six',masterCommission:'Gold Coin (1%)',categoryCommission:'Gold Jewellery (19%)',APIKey:'123416',status:'active'},
        {partnerName:'partner name seven',masterCommission:'Gold Coin (1%)',categoryCommission:'Gold Jewellery (19%)',APIKey:'123416',status:'active'},
        {partnerName:'partner name eight',masterCommission:'Gold Coin (5%)',categoryCommission:'Gold Jewellery (19%)',APIKey:'123456',status:'active'},
        {partnerName:'partner name nine',masterCommission:'Gold Coin (15%)',categoryCommission:'Gold Jewellery (19%)',APIKey:'123456',status:'active'},
        {partnerName:'partner name ten',masterCommission:'Gold Coin (15%)',categoryCommission:'Gold Jewellery (19%)',APIKey:'123456ed',status:'active'},
        {partnerName:'partner name eleven',masterCommission:'Gold Coin (5%)',categoryCommission:'Gold Jewellery (19%)',APIKey:'123456ed',status:'active'},
        {partnerName:'partner name twelve',masterCommission:'Gold Coin (15%)',categoryCommission:'Gold Jewellery (19%)',APIKey:'123456',status:'active'}
    ])

    useEffect(() => {
        const timer = setTimeout(() => {
             if(isfilter && selectedState!==undefined){
            var searchData = rows.filter(data => (
                data[selectedState].toLowerCase().indexOf(state[selectedState].toLowerCase()) !==-1
            ))
                setRows(searchData)
            console.log(searchData)
            // console.log()
    }
},2000)
return (()=> clearTimeout(timer))
    })

    function usePrevious(value) {
        const ref = useRef();
        useEffect(() => {
          ref.current = value;
        });
        return ref.current;
      }

      const prevState=usePrevious(rows)
      console.log(prevState)

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
      };
    
      const handleChangeRowsPerPage = event => {
        setRowsPerPage(+event.target.value);
        setPage(0);
      };

    const columns =[
        {id:'1',label:'Partner Name'},
        {id:'2',label:'Master Commission'},
        {id:'3',label:'Category Commission'},
        {id:'4',label:'API Key'},
        {id:'5',label:'Status'},
        {id:'6',label:'Edit'},
        {id:'7',label:'Delete'}
    ]

    const search =[
        {id:'1',label:'partnerName'},
        {id:'2',label:'masterCommission'},
        {id:'3',label:'categoryCommission'},
        {id:'4',label:'APIKey'},
        {id:'5',label:'status'},
        {id:'6',label:'edit'},
        {id:'7',label:'delete'}
    ]
    const handleChange = (e) =>{

        const {name,value}=e.target
        setIsfilter(true)
        setState(prevState => ({ ...prevState, [name]: value }))
        setSelectedState(name)
    }
    return (
        <div>
            <TableReuse>
                <TableHead>
                    <TableRow>
                    {columns.map(column => (
                        <TableCell 
                            key={column.id}
                        >
                            {column.label}
                        </TableCell>
                    ))}
                    </TableRow>
                    <TableRow>
                        {search.map((search,i) =>(
                            <TableCell
                            key={i}>
                                <TextField
                                    type="text"
                                    name={search.label}
                                    onChange={handleChange}
                                    placeholder={`search ${search.label}`}
                                />
                            </TableCell>
                            
                        ))}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.slice(page * rowsPerPage,page * rowsPerPage + rowsPerPage).map((row,i) =>{
                        return(
                            <TableRow hover role="checkbox" key={i} tabIndex={-1}>
                                <TableCell>{row.partnerName}</TableCell>
                                <TableCell>{row.masterCommission}</TableCell>
                                <TableCell>{row.categoryCommission}</TableCell>
                                <TableCell>{row.APIKey}</TableCell>
                                <TableCell>{row.status}</TableCell>
                                <TableCell><EditOutlinedIcon /></TableCell>
                                <TableCell><DeleteOutlineOutlinedIcon /></TableCell>
                            </TableRow>
                        )
                    })}
                </TableBody>
            </TableReuse>
            <Paper className={classes.boxShadow}>
                <TablePagination
                    rowsPerPageOptions={[10, 25, 100]}
                    component="div"
                    count={rows.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                />
            </Paper>
        </div>
    )
}
