import React from 'react'
import { Formik } from 'formik'

export default function ChangePassword() {
    return (
        <Formik
            initialValues={{
                oldPassword:'',
                newPassword:'',
                confirmPassword:''
            }}
        >
            {({values,errors,touched,handleChange,handleBlur,handleSubmit,isSubmitting})=>(
                <form
                    autoComplete="off"
                    className="kt-form"
                    onSubmit={handleSubmit}
                >
                    <div className="form-group">
                        <TextField
                            type="oldPassword"
                            label="Old Password"
                            margin="normal"
                            className={touched.oldPassword && errors.oldPassword ? "has-error kt-width-full":"kt-width-full"}
                            name="oldPassword"
                            onBlur={handleBlur}
                            onChange={handleChange}
                            value={values.oldPassword}
                        />
                    </div>
                    <ErrorValidation touched={touched.oldPassword} message={errors.oldPassword} />

                    <div className="form-group">
                        <TextField
                            type="newPassword"
                            label="New Password"
                            margin="normal"
                            className={touched.newPassword && errors.newPassword ? "has-error kt-width-full":"kt-width-full"}
                            name="newPassword"
                            onBlur={handleBlur}
                            onChange={handleChange}
                            value={values.newPassword}
                        />
                    </div>
                    <ErrorValidation touched={touched.newPassword} message={errors.newPassword} />

                    <div className="form-group">
                        <TextField
                            type="confirmPassword"
                            label="Confirm Password"
                            margin="normal"
                            className={touched.confirmPassword && errors.confirmPassword ? "has-error kt-width-full":"kt-width-full"}
                            name="confirmPassword"
                            onBlur={handleBlur}
                            onChange={handleChange}
                            value={values.confirmPassword}
                        />
                    </div>
                    <ErrorValidation touched={touched.confirmPassword} message={errors.confirmPassword} />
                </form>
            )}
        </Formik>
    )
}
