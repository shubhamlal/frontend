import React, { useEffect, useState } from 'react'
import ClearRoundedIcon from '@material-ui/icons/ClearRounded';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles';
import { FormattedMessage, injectIntl } from "react-intl";
import VisibilityOutlinedIcon from '@material-ui/icons/VisibilityOutlined'
import TableReuse from '../home/table/TableReuse';
import { TableHead, Modal, TableRow, TableCell, TableBody, Paper, TablePagination, TextField } from '@material-ui/core';
import CreateOutlinedIcon from '@material-ui/icons/CreateOutlined';
import DeleteOutlinedIcon from '@material-ui/icons/DeleteOutlined';
import Grid from '@material-ui/core/Grid';




const useStyles = makeStyles((theme) => ({
    boxShadow: {
        boxShadow: "none !important"
    },
    flex: {
        display: 'flex',
        cursor: 'pointer'
    },
    margin: {
        marginRight: '10px'
    },
    button: {
        marginBottom: "2%"
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        // background: 'rgba(0,0,0,0.6)'
        backgroundolor: "#fffff",
        opacity: ".7",

    },
    crossicon: {
        position: 'absolute',
        right: '0',
        top: '0',
        padding: '10px',
        transform: 'scale(1.2)',
        cursor: 'pointer'

    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
        position: "relative",
        height: "40%",
        width: "40%"
    },
}))

export default function CatalogPermission() {
    //     STATE      //
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);
    const [columns] = useState([
        { name: 'Merchants' },
        { name: 'Action' }
    ])
    const [rows] = useState([
        { PartnerName: "Merchant1" },
        { PartnerName: "Merchant1" },
        { PartnerName: "itcsd jadgui  " },
        { PartnerName: "Merchant1" },
    ])
    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };
    const handleOpen = () => {
        setOpen(!open);

    };
    const handleChangeRowsPerPage = event => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };

    return (
        <div>

            <div style={{ textAlign: 'end' }}>
                {/* <div className="kt-login__title" style={{ textAlign: "center" }}>
                    <h4><FormattedMessage id="AUTH.CATALOGPERMISSION.TITLE" />  </h4>
                </div> */}
                <Link to="/create-new-merchant">
                    <Button variant="contained" color="primary" className={classes.button}>
                        create new merchant</Button>
                </Link>
            </div>

            <div>
                <TableReuse>
                    <TableHead>
                        <TableRow>
                            {columns.map((data, i) => (
                                <TableCell key={i}>
                                    {data.name}
                                </TableCell>
                            ))}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row, i) => {

                            return (
                                <TableRow key={rows.id}>
                                    <TableCell>{row.PartnerName}</TableCell>
                                    <TableCell>
                                        <div className={classes.flex}>
                                            <div>
                                                <Modal
                                                    aria-labelledby="transition-modal-title"
                                                    aria-describedby="transition-modal-description"
                                                    className={classes.modal}
                                                    open={open}
                                                    onClose={handleOpen}
                                                >
                                                    <div className={classes.paper}>
                                                        <div className={classes.crossicon} onClick={handleOpen}>
                                                            <ClearRoundedIcon />
                                                        </div>
                                                        <h4 >Add new Role</h4>
                                                        <div >
                                                            <Grid container spacing={4} >
                                                                <Grid item>
                                                                    <p>Partner name</p>
                                                                    <p>Permissions</p>
                                                                </Grid>
                                                                <Grid item style={{ margin: "auto" }}>
                                                                    <p >Itzcash Partner</p>
                                                                    <p>Gold Coin</p>
                                                                    <p>Gold Coin</p>
                                                                    <p>Gold Coin</p>
                                                                    <p>Gold Coin</p>
                                                                </Grid>
                                                            </Grid>
                                                        </div>



                                                    </div></Modal>
                                            </div>
                                            <VisibilityOutlinedIcon className={classes.margin} onClick={handleOpen} />
                                            < CreateOutlinedIcon className={classes.margin} />
                                            <DeleteOutlinedIcon />
                                        </div>

                                    </TableCell>
                                </TableRow>
                            )
                        })}
                    </TableBody>
                </TableReuse>
                <Paper className={classes.boxShadow}>
                    <TablePagination
                        rowsPerPageOptions={[10, 25, 100]}
                        component="div"
                        count={rows.length}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        onChangePage={handleChangePage}
                        onChangeRowsPerPage={handleChangeRowsPerPage}
                    />
                </Paper>
            </div>
        </div>
    )
}


