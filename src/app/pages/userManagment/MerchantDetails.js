import React from 'react'
import { Button } from '@material-ui/core'
import { Link } from 'react-router-dom'
import PartnersTables from './MerchantsTable'

export default function PartnerDetails() {
    return (
        <div>
            <div className="textRight">
                <Link to="/create-merchant">
                    <Button variant="contained" color="primary">Create Merchant</Button>
                </Link>
            </div>

                <PartnersTables />

        </div>
    )
}
