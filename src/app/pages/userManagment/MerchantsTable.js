import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles';
import TableReuse from '../home/table/TableReuse'
import { TableHead, TableRow, TableCell, TableBody, Paper, TablePagination } from '@material-ui/core'
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import DeleteOutlineOutlinedIcon from '@material-ui/icons/DeleteOutlineOutlined';

const useStyles = makeStyles({
    boxShadow:{
        boxShadow:"none !important"
    },
    flex:{
        display:'flex',
        cursor:'pointer'
    }
  });

export default function PartnersTables() {
    const classes = useStyles();
    const [columns]=useState([
        {id:1,name:'Sr. No'},
        {id:2,name:'Design Image'},
        {id:3,name:'SKU Code'},
        {id:4,name:'Product Name'},
        {id:5,name:'Category'},
        {id:6,name:'Sub-Category'},
        {id:7,name:'Weight'},
        {id:8,name:'Purity'},
        {id:9,name:'Karat'},
        {id:10,name:'Jewellery Type'},
        {id:11,name:'Price'},
        {id:12,name:'Action'},
    ])
    const [data]=useState([
        {id:1,image:'image',skuCode:'12345',productName:'product name',category:'category1',subCategory:'sub category2',weight:'23',purity:'23',karat:'24',jewelleryType:'type1',price:'12345'},
        {id:2,image:'image2',skuCode:'12ac345',productName:'product__name',category:'category2',subCategory:'sub category1',weight:'100',purity:'20',karat:'20',jewelleryType:'type12',price:'122345'},
        {id:3,image:'3image',skuCode:'123vc45',productName:'second product name',category:'category1',subCategory:'sub category',weight:'223',purity:'13',karat:'14',jewelleryType:'type2',price:'123345'},
        {id:1,image:'image',skuCode:'12345',productName:'product name',category:'category1',subCategory:'sub category2',weight:'23',purity:'23',karat:'24',jewelleryType:'type1',price:'12345'},
        {id:2,image:'image2',skuCode:'12ac345',productName:'product__name',category:'category2',subCategory:'sub category1',weight:'100',purity:'20',karat:'20',jewelleryType:'type12',price:'122345'},
        {id:3,image:'3image',skuCode:'123vc45',productName:'second product name',category:'category1',subCategory:'sub category',weight:'223',purity:'13',karat:'14',jewelleryType:'type2',price:'123345'},
        {id:1,image:'image',skuCode:'12345',productName:'product name',category:'category1',subCategory:'sub category2',weight:'23',purity:'23',karat:'24',jewelleryType:'type1',price:'12345'},
        {id:2,image:'image2',skuCode:'12ac345',productName:'product__name',category:'category2',subCategory:'sub category1',weight:'100',purity:'20',karat:'20',jewelleryType:'type12',price:'122345'},
        {id:3,image:'3image',skuCode:'123vc45',productName:'second product name',category:'category1',subCategory:'sub category',weight:'223',purity:'13',karat:'14',jewelleryType:'type2',price:'123345'},
        {id:1,image:'image',skuCode:'12345',productName:'product name',category:'category1',subCategory:'sub category2',weight:'23',purity:'23',karat:'24',jewelleryType:'type1',price:'12345'},
        {id:2,image:'image2',skuCode:'12ac345',productName:'product__name',category:'category2',subCategory:'sub category1',weight:'100',purity:'20',karat:'20',jewelleryType:'type12',price:'122345'},
        {id:3,image:'3image',skuCode:'123vc45',productName:'second product name',category:'category1',subCategory:'sub category',weight:'223',purity:'13',karat:'14',jewelleryType:'type2',price:'123345'},
        {id:1,image:'image',skuCode:'12345',productName:'product name',category:'category1',subCategory:'sub category2',weight:'23',purity:'23',karat:'24',jewelleryType:'type1',price:'12345'},
        {id:2,image:'image2',skuCode:'12ac345',productName:'product__name',category:'category2',subCategory:'sub category1',weight:'100',purity:'20',karat:'20',jewelleryType:'type12',price:'122345'},
        {id:3,image:'3image',skuCode:'123vc45',productName:'second product name',category:'category1',subCategory:'sub category',weight:'223',purity:'13',karat:'14',jewelleryType:'type2',price:'123345'}
    ])
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
      };
    
      const handleChangeRowsPerPage = event => {
        setRowsPerPage(+event.target.value);
        setPage(0);
      };

      console.log(page * rowsPerPage+1,page * rowsPerPage + rowsPerPage)
      console.log(page,rowsPerPage)

    return (
        <div>
            <TableReuse>
                <TableHead>
                    <TableRow>
                        {columns.map(col=>(
                            <TableCell key={col.id}>
                                {col.name}
                            </TableCell>
                        ))}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {data.slice(page * rowsPerPage+1,page * rowsPerPage + rowsPerPage).map((data,i) =>(
                        <TableRow key={i}>
                            <TableCell>{i+1}</TableCell>
                            <TableCell>{data.image}</TableCell>
                            <TableCell>{data.skuCode}</TableCell>
                            <TableCell>{data.productName}</TableCell>
                            <TableCell>{data.category}</TableCell>
                            <TableCell>{data.subCategory}</TableCell>
                            <TableCell>{data.weight}</TableCell>
                            <TableCell>{data.purity}</TableCell>
                            <TableCell>{data.karat}</TableCell>
                            <TableCell>{data.jewelleryType}</TableCell>
                            <TableCell>{data.price}</TableCell>
                            <TableCell>
                                <div className={classes.flex}>
                                    <EditOutlinedIcon />
                                    <DeleteOutlineOutlinedIcon />
                                </div>
                            </TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </TableReuse>
            <Paper className={classes.boxShadow}>
                <TablePagination
                    rowsPerPageOptions={[1, 10, 100]}
                    component="div"
                    count={data.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                />
            </Paper>
        </div>
    )
}
