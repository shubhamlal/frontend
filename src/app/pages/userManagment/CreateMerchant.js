import React, { useState } from 'react'
import { Formik } from 'formik'
import * as Yup from 'yup'
import { TextField, FormControl, InputLabel, NativeSelect, Grid, Button,Backdrop,Fade,Modal } from '@material-ui/core'
import ErrorValidation from '../errors/ErrorValidation'
import { makeStyles } from '@material-ui/core/styles';
import ClearRoundedIcon from '@material-ui/icons/ClearRounded';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers'
import DateFnsUtils from '@date-io/date-fns'


const useStyles = makeStyles(theme => ({
    formControl: {
        margin: theme.spacing(1),
        width:'100%'
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        background:'rgba(0,0,0,0.6)'
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
        textAlign:'center',
        position:'relative'
      },
  }));

export default function CreatePartner() {
    const classes = useStyles();

    const [partner,setPartner]=useState()
    const [modal,setModal]=useState(false)
    const [error,setError]=useState(false)
    const [API,setAPI]=useState()
    console.log(API)
    console.log(partner)
    console.log(error)

    const validationSchema = Yup.object().shape({
        name:Yup.string()
        .required("Partner Name is required"),

        masterCommission:Yup.string().required("master commission is required"),

        categoryCommission:Yup.string().required("category commission is required"),

        masterPer:Yup.number()
        .positive("enter only positive number")
        .required("master percentage is required"),

        categoryPer:Yup.number()
        .positive("enter only positive number")
        .required("category percentage is required"),

        // api:Yup.object()
        //     .required("API key is required")
    });

    const validationSchemaAPI = Yup.object().shape({

        api:Yup.string()
        .required("API Key is required"),

        createdBy:Yup.string()
        .required("Created By filed is required"),

        status:Yup.string()
        .required("Status is required")

        // api:Yup.array()
    });

    const handleModal=e=>{
        setModal(!modal)
    }
    
    return (
        <div className="createUserForm">
            <h3>Create new Merchant</h3>

            <Formik
                initialValues={{
                    name:'',
                    masterCommission:'',
                    categoryCommission:'',
                    masterPer:'',
                    categoryPer:''
                }}
                
                validationSchema={validationSchema}

                onSubmit={(values,{resetForm,setSubmitting})=>{
                    console.log(values)
                    console.log(error)
                    setSubmitting(true);
                    // if((API !==""||API!==undefined) && !error ){
                    const value={
                        partnerName:values.name,
                        masterCommission:values.masterCommission,
                        categoryCommission:values.categoryCommission,
                        masterPer:values.masterPer,
                        categoryPer:values.categoryPer,
                        api:{
                            apiKey:API.api,
                            createdBy:API.createdBy,
                            createDate:API.createDate,
                            status:API.status
                        }
                    }

                    setTimeout(()=>{
                        setPartner(value)
                        resetForm()
                        setAPI()
                        setSubmitting(false)
                    },1000)
                // }else{
                //     setError(true)
                // }
                }}
            >
                {({values,errors,touched,handleChange,handleBlur,handleSubmit,isSubmitting})=>(
                    <form
                        autoComplete="off"
                        className="kt-form" 
                        onSubmit={handleSubmit}
                    >
                        <div>
                            <TextField
                                type="text"
                                label="Merchant Name"
                                margin="normal"
                                className={touched.name && errors.name ? "has-error kt-width-full":"kt-width-full"}
                                name="name"
                                onBlur={handleBlur}
                                onChange={handleChange}
                                value={values.name}
                            />
                            <ErrorValidation touched={touched.name} message={errors.name} />
                        </div>
                        <Grid container spacing={3}>
                            <Grid item xs={6}>
                                <FormControl className={classes.formControl}>
                                    <InputLabel>Master Commission</InputLabel>
                                    <NativeSelect
                                        name='masterCommission'
                                        value={values.masterCommission}
                                        onChange={handleChange}
                                        className={touched.masterCommission && errors.masterCommission ? "has-error kt-width-full":"kt-width-full"}
                                    >
                                        <option aria-label="None" value="" />
                                        <option value="gold_coin">Gold Coin</option>
                                        <option value="gold_jewellery">Gold Jewellery</option>
                                    </NativeSelect>
                                </FormControl>
                                <ErrorValidation touched={touched.masterCommission} message={errors.masterCommission} />
                            </Grid>
                            <Grid item xs={6}>
                            <TextField
                                type="number"
                                label="Commission (%)"
                                margin="normal"
                                className={touched.masterPer && errors.masterPer ? "has-error kt-width-full":"kt-width-full"}
                                name="masterPer"
                                onBlur={handleBlur}
                                onChange={handleChange}
                                value={values.masterPer}
                            />
                            <ErrorValidation touched={touched.masterPer} message={errors.masterPer} />
                            </Grid>
                        </Grid>

                        <Grid container spacing={3}>
                            <Grid item xs={6}>
                                <FormControl className={classes.formControl}>
                                    <InputLabel>Category Commission</InputLabel>
                                    <NativeSelect
                                        name='categoryCommission'
                                        value={values.categoryCommission}
                                        onChange={handleChange}
                                        className={touched.categoryCommission && errors.categoryCommission ? "has-error kt-width-full":"kt-width-full"}
                                    >
                                        <option aria-label="None" value="" />
                                        <option value="gold_coin">Gold Coin</option>
                                        <option value="gold_jewellery">Gold Jewellery</option>
                                    </NativeSelect>
                                </FormControl>
                                <ErrorValidation touched={touched.categoryCommission} message={errors.categoryCommission} />
                            </Grid>
                            <Grid item xs={6}>
                            <TextField
                                type="number"
                                label="Commission (%)"
                                margin="normal"
                                className={touched.categoryPer && errors.categoryPer ? "has-error kt-width-full":"kt-width-full"}
                                name="categoryPer"
                                onBlur={handleBlur}
                                onChange={handleChange}
                                value={values.categoryPer}
                            />
                            <ErrorValidation touched={touched.categoryPer} message={errors.categoryPer} />
                            </Grid>
                        </Grid>

                        <div style={{margin:'20px 0'}}>
                            {API==="" || API===undefined ?
                            <>
                                <Button variant="outlined" color="primary" onClick={handleModal} className={touched.api && errors.api ? "has-error kt-width-full":"kt-width-full"}>
                                    Generate API key
                                </Button>
                            </>:
                                <Button variant="outlined" color="primary" onClick={handleModal}>
                                    API Key: {API.api}
                                </Button>}
                            
                        </div>

                        <div style={{margin:'10px 0'}}>
                            <Button variant="contained" color="primary" type="submit">Create</Button>
                        </div>
                    </form> 
                )}

            </Formik>

            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={modal}
                onClose={handleModal}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                timeout: 500,
                }}
            >
                <Fade in={modal}>
                <div className={classes.paper}>
                    <div style={{position:'absolute',right:'0',top:'0',padding:'10px',transform:'scale(1.2)',cursor:'pointer'}} onClick={handleModal}>
                        <ClearRoundedIcon/>
                    </div>
                    <h4 style={{margin:'15px 0'}}>Generate API Key</h4>
                    <Formik
                        initialValues={{
                            api:'',
                            createdBy:'',
                            createDate:new Date(),
                            status:''
                        }}
                        validationSchema={validationSchemaAPI}

                        onSubmit={(values,{setSubmitting})=>{
                            setSubmitting(true);

                            setTimeout(()=>{
                                setAPI(values)
                                setModal(!modal)
                                setSubmitting(false)
                            },1000)

                            
                        }}
                    >
                        {({values,errors,touched,handleChange,handleBlur,handleSubmit,isSubmitting})=>(
                            <form
                                autoComplete="off"
                                className="kt-form" 
                                onSubmit={handleSubmit}
                            >
                                <TextField
                                    type="text"
                                    label="API Key"
                                    margin="normal"
                                    className={touched.api && errors.api ? "has-error kt-width-full":"kt-width-full"}
                                    name="api"
                                    onBlur={handleBlur}
                                    onChange={handleChange}
                                    value={values.api}
                                />
                                <ErrorValidation touched={touched.api} message={errors.api} />

                                <TextField
                                    type="text"
                                    label="Created By"
                                    margin="normal"
                                    className={touched.createdBy && errors.createdBy ? "has-error kt-width-full":"kt-width-full"}
                                    name="createdBy"
                                    onBlur={handleBlur}
                                    onChange={handleChange}
                                    value={values.createdBy}
                                />
                                <ErrorValidation touched={touched.createdBy} message={errors.createdBy} />

                                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                    <KeyboardDatePicker
                                        disableToolbar
                                        variant="inline"
                                        format="MM/dd/yyyy"
                                        margin="normal"
                                        id="date-picker-inline"
                                        label="Date picker inline"
                                        value={values.createDate}
                                        onChange={handleChange}
                                        KeyboardButtonProps={{
                                            'aria-label': 'change date',
                                        }}
                                    />
                                </MuiPickersUtilsProvider>
                                <ErrorValidation touched={touched.createDate} message={errors.createDate} />

                                <FormControl className={classes.formControl}>
                                    <InputLabel>Category Commission</InputLabel>
                                    <NativeSelect
                                        name='status'
                                        value={values.status}
                                        onChange={handleChange}
                                        className={touched.status && errors.status ? "has-error kt-width-full":"kt-width-full"}
                                    >
                                        <option aria-label="None" value="" />
                                        <option value="active">Active</option>
                                        <option value="deactive">Deactive</option>
                                    </NativeSelect>
                                </FormControl>

                                <Button variant="contained" color="primary" type="submit" disabled={isSubmitting}>
                                    Create
                                </Button>
                            </form>
                        )}
                    </Formik>
                </div>
                </Fade>
            </Modal>
        </div>
    )
}
