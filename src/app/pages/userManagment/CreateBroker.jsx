
import React, { useState, useEffect } from "react";
import { Formik } from "formik";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import APIServices from "../../API/APIServices"
import { STATES, CITIES, BASE_URL } from "../../API/APIEndpoints"
import { FormattedMessage, injectIntl } from "react-intl";
import ArrowBackOutlinedIcon from '@material-ui/icons/ArrowBackOutlined';
import { MenuItem, Select, TextField, makeStyles, FormControl, InputLabel } from "@material-ui/core";
import * as auth from "../../store/ducks/auth.duck";


function CreateBroker(props) {
    const { intl } = props;
    const [vaUserStatus, setvaUserStatus] = useState('');
    const [stateId, setStateId] = useState(undefined)
    const [cityId, setCityId] = useState(0)
    const [state, setState] = useState([]);
    const [city, setCity] = useState([]);

    const useStyles = makeStyles(theme => ({
        formControl: {

            minWidth: "100%"

        }
    }));

    const handledropdown = e => {
        setvaUserStatus(e.target.name = e.target.value);

    };
    const handleStateChange = e => {
        let index = e.target.selectedIndex
        setStateId(e.target.value)



    }
    const handleCityChange = e => {
        setCityId(e.target.value)

    }

    async function getStates() {
        const url = BASE_URL + STATES
        const resp = await new APIServices().get(url)
        const data = resp.results.message
        setState(data);
    }
    async function getCities(id) {
        const url = BASE_URL + CITIES + id
        const resp = await new APIServices().get(url)
        const data = resp.results.message
        setCity(data)
    }

    useEffect(() => {


        if (stateId !== undefined) {
            getCities(stateId)
        }
        else {
            getStates()
        }

    }, [stateId])




    const classes = useStyles()
    return (
        <div>
            <div className="kt-login__body">
                <h4>
                    <Link to="/broker-details">
                        <ArrowBackOutlinedIcon />
                        &nbsp;  back

                            </Link>
                </h4>
                <div className="kt-login__form" style={{ width: "50%", margin: "auto" }}>


                    <div className="kt-login__title" style={{ textAlign: "center" }}>
                        <h3>

                            <FormattedMessage id="AUTH.CREATEBROKER.TITLE" />
                        </h3>
                    </div>

                    <Formik
                        initialValues={{

                            fullname: "",
                            userEmail: "",
                            mobileNo: "",
                            address: "",

                            city: "",
                            pinCode: "",
                            storeId: "",
                            // vaUserStatus: "",

                        }}
                        validate={values => {
                            const errors = {};

                            // if (!values.email) {
                            //     errors.email = intl.formatMessage({
                            //         id: "AUTH.VALIDATION.REQUIRED_FIELD"
                            //     });
                            // } 
                            if (values.userEmail && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.userEmail)
                            ) {
                                errors.userEmail = intl.formatMessage({
                                    id: "AUTH.VALIDATION.INVALID_FIELD"
                                });
                            }

                            if (values.mobileNo && (values.mobileNo.length > 10 || values.mobileNo.length < 10 || isNaN(values.mobileNo))) {
                                errors.mobileNo = intl.formatMessage({
                                    id: "AUTH.VALIDATION.MOBILENO"
                                });
                            }
                            if (values.pinCode && (values.pinCode.length > 6 || values.pinCode.length < 6 || isNaN(values.pinCode))) {
                                errors.pinCode = intl.formatMessage({
                                    id: "AUTH.VALIDATION.PINCODE"
                                });
                            }



                            return errors;
                        }}
                    // onSubmit={(values, { setStatus, setSubmitting }) => {
                    //     register(
                    //         values.email,
                    //         values.fullname,
                    //         values.username,
                    //         values.password
                    //     )
                    //         .then(({ data: { accessToken } }) => {
                    //             props.register(accessToken);
                    //         })
                    //         .catch(() => {
                    //             setSubmitting(false);
                    //             setStatus(
                    //                 intl.formatMessage({
                    //                     id: "AUTH.VALIDATION.INVALID_LOGIN"
                    //                 })
                    //             );
                    //         });
                    // }}
                    >
                        {({
                            values,
                            status,
                            errors,
                            touched,
                            handleChange,
                            handleBlur,
                            handleSubmit,
                            isSubmitting
                        }) => (
                                <form onSubmit={handleSubmit} noValidate autoComplete="off">
                                    {status && (
                                        <div role="alert" className="alert alert-danger">
                                            <div className="alert-text">{status}</div>
                                        </div>
                                    )}

                                    <div className="form-group mb-0">
                                        <TextField
                                            margin="normal"
                                            label="Fullname"
                                            className="kt-width-full"
                                            name="fullname"
                                            onBlur={handleBlur}
                                            onChange={handleChange}
                                            value={values.fullname}
                                            helperText={touched.fullname && errors.fullname}
                                            error={Boolean(touched.fullname && errors.fullname)}
                                        />
                                    </div>

                                    <div className="form-group mb-0">
                                        <TextField
                                            label="User email"
                                            margin="normal"
                                            className="kt-width-full"
                                            name="userEmail"
                                            onBlur={handleBlur}
                                            onChange={handleChange}
                                            value={values.userEmail}
                                            helperText={touched.userEmail && errors.userEmail}
                                            error={Boolean(touched.userEmail && errors.userEmail)}
                                        />
                                    </div>

                                    <div className="form-group mb-0">
                                        <TextField
                                            margin="normal"
                                            label="Mobile.no"
                                            className="kt-width-full"
                                            name="mobileNo"
                                            onBlur={handleBlur}
                                            onChange={handleChange}
                                            value={values.mobileNo}
                                            helperText={touched.mobileNo && errors.mobileNo}
                                            error={Boolean(touched.mobileNo && errors.mobileNo)}
                                        />
                                    </div>

                                    <div className="form-group mb-0" style={{ paddingBottom: "2%" }}>
                                        <TextField
                                            margin="normal"
                                            label="Address"
                                            className="kt-width-full"
                                            name="address"
                                            onBlur={handleBlur}
                                            onChange={handleChange}
                                            value={values.address}
                                            helperText={touched.address && errors.address}
                                            error={Boolean(touched.address && errors.address)}
                                        />
                                    </div>


                                    <div className="form-group mb-0">
                                        <FormControl className={classes.formControl} style={{ paddingBottom: "2%" }}>
                                            <InputLabel ClassName="kt-width-full" id="demo-simple-select-label">State</InputLabel>
                                            <Select
                                                labelId="demo-simple-select-label"
                                                id="simple-select-label"
                                                name="state"
                                                value={state.id}
                                                onChange={handleStateChange}
                                            >
                                                <MenuItem value={0} >None</MenuItem>
                                                {state && state.map((data) => (
                                                    <MenuItem key={data.id} value={data.id}>
                                                        {data.state_name}
                                                    </MenuItem>

                                                ))}


                                            </Select>
                                        </FormControl>
                                    </div>
                                    <div className="form-group mb-0">
                                        <FormControl className={classes.formControl} >
                                            <InputLabel ClassName="kt-width-full" id="demo-simple-select-label">City</InputLabel>
                                            <Select
                                                labelId="demo-simple-select-label"
                                                id="simple-select-label"
                                                name="city"
                                                value={city.id}
                                                onChange={handleCityChange}
                                            >
                                                <MenuItem value={0} >None</MenuItem>
                                                {city && city.map(data => (
                                                    <MenuItem key={data.id} value={data.id}>
                                                        {data.cityName}
                                                    </MenuItem>
                                                ))}

                                            </Select>
                                        </FormControl>
                                    </div>

                                    <div className="form-group mb-0">
                                        <TextField
                                            margin="normal"
                                            label="Pincode"
                                            className="kt-width-full"
                                            name="pinCode"
                                            onBlur={handleBlur}
                                            onChange={handleChange}
                                            value={values.pinCode}
                                            helperText={touched.pinCode && errors.pinCode}
                                            error={Boolean(
                                                touched.pinCode && errors.pinCode
                                            )}
                                        />
                                    </div>
                                    <div className="form-group mb-0" style={{ paddingBottom: "2%" }}>
                                        <TextField
                                            margin="normal"
                                            label="StoreId"
                                            className="kt-width-full"
                                            name="storeId"
                                            onBlur={handleBlur}
                                            onChange={handleChange}
                                            value={values.storeId}
                                            helperText={touched.storeId && errors.storeId}
                                            error={Boolean(
                                                touched.storeId && errors.storeId
                                            )}
                                        />
                                    </div>
                                    <div className="form-group mb-0" >
                                        <FormControl className={classes.formControl} style={{ paddingBottom: "3%" }}>
                                            <InputLabel ClassName="kt-width-full" id="demo-simple-select-label">Va User Status</InputLabel>
                                            <Select
                                                labelId="demo-simple-select-label"
                                                id="simple-select-label"
                                                name="vaUserStatus"
                                                value={vaUserStatus}
                                                onChange={handledropdown}
                                            >
                                                <MenuItem value={1}>Active</MenuItem>
                                                <MenuItem value={2}>Inactive</MenuItem>
                                                <MenuItem value={3}>Pending</MenuItem>
                                            </Select>
                                        </FormControl>
                                    </div>


                                    <button
                                        disabled={isSubmitting}
                                        className="btn btn-primary btn-elevate kt-login__btn-primary"

                                    >
                                        Submit
                </button>
                                </form>
                            )}
                    </Formik>
                </div>
            </div>
        </div>
    );
}








export default injectIntl(
    connect(
        null,
        auth.actions
    )(CreateBroker)
)



