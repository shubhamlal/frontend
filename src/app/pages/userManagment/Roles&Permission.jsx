import React, { useEffect, useState, useReducer } from 'react'
import ErrorValidation from '../errors/ErrorValidation';
import { Formik } from 'formik'
import * as Yup from 'yup'
import clsx from "clsx";
import ClearRoundedIcon from '@material-ui/icons/ClearRounded';
import { BASE_URL, ROLES } from "../../API/APIEndpoints"
import APIServices from "../../API/APIServices"
import { makeStyles } from '@material-ui/core/styles';
import VisibilityOutlinedIcon from '@material-ui/icons/VisibilityOutlined'
import TableReuse from '../home/table/TableReuse';
import { Backdrop, TableHead, TableRow, TableCell, TableBody, Paper, TablePagination, TextField, Modal, Fade, Button } from '@material-ui/core';
import CreateOutlinedIcon from '@material-ui/icons/CreateOutlined';
import DeleteOutlinedIcon from '@material-ui/icons/DeleteOutlined';
import actions from "../../Actions/Actions."
import cogoToast from "cogo-toast";
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import { Dialog, DialogTitle, DialogContent, DialogActions } from '@material-ui/core';
import { FormControl, InputLabel, Select, Input, Chip, MenuItem, Checkbox } from '@material-ui/core';
const useStyles = makeStyles((theme) => ({
    boxShadow: {
        boxShadow: "none !important"
    },
    flex: {
        display: 'flex',
        cursor: 'pointer'
    },
    margin: {
        marginRight: '10px'
    },

    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        background: 'rgba(0,0,0,0.6)'
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
        position: "relative"
    },
    chips: {
        display: "flex",
        flexWrap: "wrap",
        width: "100%"
    },
    chip: {
        margin: 2
    },
    noLabel: {
        marginTop: theme.spacing(3)
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 500,
        maxWidth: 1000
    },
}));

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250
        }
    }
};
const names = [
    "Oliver Hansen",
    "Van Henry",
    "April Tucker",
    "Ralph Hubbard",
    "Omar Alexander",
    "Carlos Abbott",
    "Miriam Wagner",
    "Bradley Wilkerson",
    "Virginia Andrews",
    "Kelly Snyder"
];

function getStyles(name, personName, theme) {
    return {
        fontWeight:
            personName.indexOf(name) === -1
                ? theme.typography.fontWeightRegular
                : theme.typography.fontWeightMedium
    };
}

//INITIAL STATE FOR REDUCER FUNCTION 

const initialState = {
    rows: {},
    error: {}
}

//REDUCER FUNCTION TO PERFORM ACTIONS

const reducerFunction = (state, action) => {
    switch (action.type) {
        case actions.GET_ROLES_SUCCESS:
            return {
                error: {},
                rows: action.payload
            }
        case actions.GET_ROLES_FAILURE:
            return {
                rows: {},
                error: cogoToast.error('Something went wrong')
            }

        default:
            return state
    }
}
export default function RolesPermission() {
    const [state, dispatch] = useReducer(reducerFunction, initialState)
    const classes = useStyles();
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);
    const [loadingButtonStyle, setLoadingButtonStyle] = useState({
        paddingRight: "2.5rem"
    });
    const [loading, setLoading] = useState(false);
    const [open, setOpen] = React.useState(false);
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

    const [personName, setPersonName] = React.useState([]);

    const handleChange = event => {
        setPersonName(event.target.value);
    };

    const handleChangeMultiple = event => {
        const { options } = event.target;
        const value = [];
        for (let i = 0, l = options.length; i < l; i += 1) {
            if (options[i].selected) {
                value.push(options[i].value);
            }
        }
        setPersonName(value);
    };

    const [columns] = useState([
        { name: 'Roles' },
        { name: 'Permission' }
    ])
    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };


    //validation
    const validationSchema = Yup.object().shape({
        categoryName: Yup.string()
            .required("role Name is Required")
    })
    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = event => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };

    // DELETE ROLE API
    const deleteRole = async (role_id) => {
        const deleteRoleUrl = BASE_URL + ROLES + '/' + role_id
        const delResp = await new APIServices().delete(deleteRoleUrl)
        console.log('this is response from delete api', delResp)
        getRoles()
    }
    // GET ROLES API 

    async function getRoles() {

        const url = BASE_URL + ROLES
        const resp = await new APIServices().get(url)
        if (resp.error) {
            dispatch({ type: "GET_ROLES_FAILURE" })
        }
        else {
            dispatch({ type: "GET_ROLES_SUCCESS", payload: resp.results })
        }

    }
    useEffect(() => {

        getRoles()
    }, [])


    return (
        <div>
            <div>

                <Dialog
                    fullScreen={fullScreen}
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="responsive-dialog-title"
                >
                    <DialogTitle id="responsive-dialog-title">Add New Role </DialogTitle>
                    <DialogContent>
                        <TextField
                            autoFocus
                            margin="dense"
                            id="roleNmae"
                            label="Role Name"
                            type="text"
                            fullWidth
                        />
                        <FormControl className={classes.formControl}>
                            <InputLabel id=" demo-mutiple-checkbox-label">permission</InputLabel>
                            <Select

                                labelId="demo-mutiple-checkbox-label"
                                id="demo-mutiple-chip"
                                multiple
                                value={personName}
                                onChange={handleChange}
                                input={<Input id="select-multiple-chip" />}
                                renderValue={selected => (
                                    <div className={classes.chips}>
                                        {selected.map(value => (
                                            <Chip key={value} label={value} className={classes.chip} />
                                        ))}
                                    </div>
                                )}
                                MenuProps={MenuProps}
                            >
                                {names.map(name => (
                                    <MenuItem
                                        key={name}
                                        value={name}
                                        style={getStyles(name, personName, theme)}

                                    >
                                        {" "}
                                        <Checkbox checked={personName.indexOf(name) > -1} />
                                        {name}
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </DialogContent>
                    <DialogActions>
                        <button id="kt_login_signin_submit"
                            className="btn btn-primary btn-elevate kt-login__btn-primary"
                            onClick={handleClose} color="primary">
                            cancel
                        </button>
                        <button
                            id="kt_login_signin_submit"
                            type="submit"
                            // disabled={isSubmitting}
                            className={`btn btn-primary btn-elevate kt-login__btn-primary ${clsx(
                                {
                                    "kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light": loading
                                }
                            )}`}
                            style={loadingButtonStyle}
                        >
                            Add Role
                                </button>
                    </DialogActions>
                </Dialog>
                {/* <Modal
                    aria-labelledby="transition-modal-title"
                    aria-describedby="transition-modal-description"
                    className={classes.modal}
                    open={open}
                    onClose={handleOpen}
                >
                    <div className={classes.paper}>
                        <div style={{ position: 'absolute', right: '0', top: '0', padding: '10px', transform: 'scale(1.2)', cursor: 'pointer' }} onClick={handleOpen}>
                            <ClearRoundedIcon />
                        </div>
                        <h4 style={{ margin: '15px 0', backgroundColor: "light blue" }}>Add new Role</h4>
                        <Formik
                            initialValues={{ newRole: "" }}
                            validationSchema={validationSchema}
                            className={classes.paper}>
                            {({ values, errors, touched, handleChange, handleBlur, handleSubmit, isSubmitting }) => (
                                <form
                                    autoComplete="off"
                                    className="kt-form"
                                    onSubmit={handleSubmit}
                                >
                                    <div className="form-group">
                                        <TextField
                                            type="roleName"
                                            label="Role Name"
                                            margin="normal"
                                            className={touched.roleName && errors.roleName ? "has-error kt-width-full" : "kt-width-full"}
                                            name="roleName"
                                            onBlur={handleBlur}
                                            onChange={handleChange}
                                            value={values.roleName}
                                        />

                                    </div>
                                    <ErrorValidation touched={touched.roleName} message={errors.roleName} />

                                    <button
                                        id="kt_login_signin_submit"
                                        type="submit"
                                        disabled={isSubmitting}
                                        className={`btn btn-primary btn-elevate kt-login__btn-primary ${clsx(
                                            {
                                                "kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light": loading
                                            }
                                        )}`}
                                        style={loadingButtonStyle}
                                    >
                                        Add Role
                                </button>
                                </form>
                            )}
                        </Formik>

                    </div></Modal> */}
            </div>



            <div onClick={handleClickOpen} className="textRight" >

                <Button variant="contained" color="primary" >
                    create new Role</Button>

            </div>

            <div>
                <TableReuse>
                    <TableHead>
                        <TableRow>
                            {columns.map((data, i) => (
                                <TableCell key={i}>
                                    {data.name}
                                </TableCell>
                            ))}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {state.rows.length > 0 && state.rows.map(row => {
                            return (
                                <TableRow key={row.id}>
                                    <TableCell>{row.roleName}</TableCell>
                                    <TableCell>

                                        <div className={classes.flex}>
                                            <VisibilityOutlinedIcon className={classes.margin} />
                                            < CreateOutlinedIcon className={classes.margin} />
                                            <div onClick={() => deleteRole(row.id)}>
                                                <DeleteOutlinedIcon />
                                            </div>
                                        </div>

                                    </TableCell>
                                </TableRow>
                            )
                        })}
                    </TableBody>
                </TableReuse>
                <Paper className={classes.boxShadow}>
                    <TablePagination
                        rowsPerPageOptions={[10, 25, 100]}
                        component="div"
                        count={state.rows.length}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        onChangePage={handleChangePage}
                        onChangeRowsPerPage={handleChangeRowsPerPage}
                    />
                </Paper>
            </div>
        </div>

    )
}







