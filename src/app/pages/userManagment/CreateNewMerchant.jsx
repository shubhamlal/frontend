import React from "react"
import { Link } from 'react-router-dom'
import { MenuItem, Select, TextField, makeStyles, FormControl, InputLabel, Button } from "@material-ui/core";
export default function CreateNewPartner() {
    const useStyles = makeStyles(theme => ({
        formControl: {

            minWidth: "100%"

        },
        formbody: {
            width: "50%",
            margin: "auto"
        },
        buttonDisplay: {
            margin: "auto",
            width: "50%",
            display: "flex",
            padding: "6%",
            justifyContent: "space-around"
        }
    }));
    const classes = useStyles()
    return (
        <div>
            <h5 style={{ textAlign: "center", padding: "3%" }}><b> New Merchant</b></h5>
            <div className={classes.formbody}>
                <div className="form-group mb-0">
                    <FormControl className={classes.formControl} >
                        <InputLabel ClassName="kt-width-full" id="demo-simple-select-label">Select New Merchant</InputLabel>
                        <Select
                            labelId="demo-simple-select-label"
                            id="simple-select-label"
                            name="Select New Partner Name"
                        // value={partnerName}
                        // onChange={handlepartnerNamechange}
                        >
                            <MenuItem value={1}>Partner1</MenuItem>
                            <MenuItem value={2}>Partner1</MenuItem>
                            <MenuItem value={3}>Partner1</MenuItem>
                        </Select>
                    </FormControl>
                </div>
                <br></br> <br></br> <br></br>
                <div className="form-group mb-0">
                    <FormControl className={classes.formControl} >
                        <InputLabel ClassName="kt-width-full" id="demo-simple-select-label">Assign Permission</InputLabel>
                        <Select
                            labelId="demo-simple-select-label"
                            id="simple-select-label"
                            name="Assign Permissions"
                        // value={partnerName}
                        // onChange={handlepartnerNamechange}
                        >
                            <MenuItem value={1}>ring</MenuItem>
                            <MenuItem value={2}>Chain</MenuItem>
                            <MenuItem value={3}>pendant</MenuItem>
                        </Select>
                    </FormControl>
                </div>

            </div>
            <div className={classes.buttonDisplay}>
                <Link to="/catalog-permission">
                    <Button /*onClick={handleCancel}*/ variant="outlined" color="primary">
                        Cancel
        </Button>
                </Link>
                <Button variant="contained" color="primary" className={classes.button}>
                    Submit
                </Button>
            </div>
        </div>)
}