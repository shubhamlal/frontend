import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles';
import TableReuse from '../home/table/TableReuse'
import { Paper, TablePagination, TableHead, TableRow, TableBody, TableCell } from '@material-ui/core'
import DeleteOutlineOutlinedIcon from '@material-ui/icons/DeleteOutlineOutlined';

const useStyles = makeStyles({
    boxShadow:{
        boxShadow:"none !important"
    },
    flex:{
        display:'flex',
        cursor:'pointer'
    }
  });
export default function OrderDetails() {
    const classes = useStyles();
    const [columns]=useState([
        {id:1,name:'Sr. No'},
        {id:2,name:'Store Id'},
        {id:3,name:'Center City'},
        {id:4,name:'Shipping Address'},
        {id:5,name:'Member Id'},
        {id:6,name:'Mobile Number'},
        {id:7,name:'Order Id'},
        {id:8,name:'Product Name'},
        {id:9,name:'Weight'},
        {id:10,name:'Order Total Amount'},
        {id:11,name:'Order Initial Amount'},
        {id:12,name:'Order Date'},
        {id:13,name:'EMI Tenure'},
        {id:14,name:'Ordered Qty'},
        {id:15,name:'Invoice No'},
        {id:16,name:'EMI Status'}
    ])
    const [data]=useState([
        {storeId:11,centerCity:'mumbai',shippingAddress:'pune',memberId:'12ws',
        mobNo:'1234567890',orderId:'12wde',productName:'name',weight:'100',orTotalAmount:'12345',
        orInitialAmount:'123456',orDate:'20-5-2020',EMITenure:'emi',orQty:'order qty',
        invoiceNo:'12wert',EMIStatus:'status'
    }
    ])
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
      };
    
      const handleChangeRowsPerPage = event => {
        setRowsPerPage(+event.target.value);
        setPage(0);
      };
    return (
        <div>
            <TableReuse>
                <TableHead>
                    <TableRow>
                        {columns.map(col=>(
                            <TableCell key={col.id}>
                                {col.name}
                            </TableCell>
                        ))}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {data.slice(page * rowsPerPage,page * rowsPerPage + rowsPerPage).map((data,i) =>(
                        <TableRow key={i}>
                        
                        </TableRow>
                    ))}
                </TableBody>
            </TableReuse>
            <Paper className={classes.boxShadow}>
                <TablePagination
                    rowsPerPageOptions={[1, 10, 100]}
                    component="div"
                    count={data.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                />
            </Paper>
        </div>
    )
}
