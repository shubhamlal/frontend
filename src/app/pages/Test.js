import React,{useState} from 'react';
import { Portlet, PortletHeader, PortletHeaderToolbar, PortletBody } from '../partials/content/Portlet';
import { Tab, Tabs } from '@material-ui/core';
import { LoginContext } from './auth/Login';

export default function Test() {
    const [tab,setTab]=useState(0)

    function handlechange(event,newValue){
        setTab(newValue);
    }
const context = React.useContext(LoginContext)
console.log(context)
    return (
        <div>
            <Portlet>
              <PortletHeader
                toolbar={
                  <PortletHeaderToolbar>
                    <Tabs
                      component="div"
                      className="builder-tabs"
                      value={tab}
                      onChange={handlechange}
                    >
                      <Tab label="Skins" />
                      {/* <Tab label="Page" /> */}
                      <Tab label="Header" />
                      <Tab label="Subheader" />
                      <Tab label="Content" />
                      <Tab label="Aside" />
                      <Tab label="Footer" />
                    </Tabs>
                  </PortletHeaderToolbar>
                }
              />
              {tab === 0 &&
              <PortletBody>
                <p>tab1 </p>
              </PortletBody>}

              {tab === 1 &&
              <PortletBody>
                <p>tab2 </p>
              </PortletBody>}

              {tab === 2 &&
              <PortletBody>
                <p>tab3 </p>
              </PortletBody>}

              {tab === 3 &&
              <PortletBody>
                <p>tab4 </p>
              </PortletBody>}
              </Portlet>

        </div>
    )
}
