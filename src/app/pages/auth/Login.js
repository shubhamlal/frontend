// import React, { useState } from "react";
// import { Link } from "react-router-dom";
// import { Formik } from "formik";
// import { connect } from "react-redux";
// import { FormattedMessage, injectIntl } from "react-intl";
// import { TextField } from "@material-ui/core";
// import clsx from "clsx";
// import * as auth from "../../store/ducks/auth.duck";
// import { login } from "../../crud/auth.crud";
// import axios from 'axios'

// function Login(props) {
//   const { intl } = props;
//   const [loading, setLoading] = useState(false);
//   const [loadingButtonStyle, setLoadingButtonStyle] = useState({
//     paddingRight: "2.5rem"
//   });
//   const[isAuthenticated,setIsAuthenticated]=useState(false)

//   const enableLoading = () => {
//     setLoading(true);
//     setLoadingButtonStyle({ paddingRight: "3.5rem" });
//   };

//   const disableLoading = () => {
//     setLoading(false);
//     setLoadingButtonStyle({ paddingRight: "2.5rem" });
//   };

//   return (
//     <>
      

//       <div className="kt-login__body">
//         <div className="kt-login__form">
//           <div className="kt-login__title">
//             <h3>
//               {/* https://github.com/formatjs/react-intl/blob/master/docs/Components.md#formattedmessage */}
//               <FormattedMessage id="AUTH.LOGIN.TITLE" />
//             </h3>
//           </div>

//           <Formik
//             initialValues={{
//               email: "admin@demo.com",
//               password: "demo"
//             }}
//             validate={values => {
//               const errors = {};

//               if (!values.email) {
//                 // https://github.com/formatjs/react-intl/blob/master/docs/API.md#injection-api
//                 errors.email = intl.formatMessage({
//                   id: "AUTH.VALIDATION.REQUIRED_FIELD"
//                 });
//               } else if (
//                 !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
//               ) {
//                 errors.email = intl.formatMessage({
//                   id: "AUTH.VALIDATION.INVALID_FIELD"
//                 });
//               }

//               if (!values.password) {
//                 errors.password = intl.formatMessage({
//                   id: "AUTH.VALIDATION.REQUIRED_FIELD"
//                 });
//               }

//               return errors;
//             }}
//             onSubmit={(values, { setStatus, setSubmitting }) => {
//               enableLoading();
//               setTimeout(() => {
//                 // login(values.email, values.password)
//                 //   .then(({ data: { accessToken } }) => {
//                 //     disableLoading();
//                 //     props.login(accessToken);
//                 //   })
//                 //   .catch(() => {
//                 //     disableLoading();
//                 //     setSubmitting(false);
//                 //     setStatus(
//                 //       intl.formatMessage({
//                 //         id: "AUTH.VALIDATION.INVALID_LOGIN"
//                 //       })
//                 //     );
//                 //   });

//                 fetch('http://173.249.49.7:9000/api/signIn', {
//                   method: "POST",
//                   headers: {
//                     "Content-Type": "application/json"
//                   },
//                   body: JSON.stringify({
//                     email: values.email,
//                     password: values.password
//                   })
//                 })
//                 .then((response) => response.json())
//                 .then(res => {
//                   const accessToken = res.token;

//                   if(accessToken !== undefined){
//                     window.localStorage.setItem("token", accessToken);

//                     setTimeout(()=>{
//                       window.location.reload()
//                     },2000)
//                   }
//                 })
//                   .catch(error => {
//                     disableLoading();
//                     setSubmitting(false);
//                     setStatus(error)
//                   } );

//                 // axios.post('173.249.49.7:9000/api/signIn',{
//                 //   email:values.email,
//                 //   password:values.password
//                 // })
//                 // .then(res=>{console.log(res)})

//               }, 1000);
//             }}
//           >
//             {({
//               values,
//               status,
//               errors,
//               touched,
//               handleChange,
//               handleBlur,
//               handleSubmit,
//               isSubmitting
//             }) => (
//               <form
//                 noValidate={true}
//                 autoComplete="off"
//                 className="kt-form"
//                 onSubmit={handleSubmit}
//               >
//                 {status ? (
//                   <div role="alert" className="alert alert-danger">
//                     <div className="alert-text">{status}</div>
//                   </div>
//                 ) : (
//                   ''
//                 )}

//                 <div className="form-group">
//                   <TextField
//                     type="email"
//                     label="Email"
//                     margin="normal"
//                     className="kt-width-full"
//                     name="email"
//                     onBlur={handleBlur}
//                     onChange={handleChange}
//                     value={values.email}
//                     helperText={touched.email && errors.email}
//                     error={Boolean(touched.email && errors.email)}
//                   />
//                 </div>

//                 <div className="form-group">
//                   <TextField
//                     type="password"
//                     margin="normal"
//                     label="Password"
//                     className="kt-width-full"
//                     name="password"
//                     onBlur={handleBlur}
//                     onChange={handleChange}
//                     value={values.password}
//                     helperText={touched.password && errors.password}
//                     error={Boolean(touched.password && errors.password)}
//                   />
//                 </div>

//                 {/* <div className="kt-login__actions"> */}
                
//                 <div style={{width:'100%',textAlign:'end',marginBottom:'20px'}}>
//                   <Link
//                     to="/auth/forgot-password"
//                     className="kt-link kt-login__link-forgot"
//                   >
//                     <FormattedMessage id="AUTH.GENERAL.FORGOT_BUTTON" />
//                   </Link>
//                 </div>
//                 <div >
//                   <Link
//                     to="/auth/change-password"
//                     className="kt-link kt-login__link-forgot"
//                   >
//                     <FormattedMessage id="AUTH.GENERAL.CHANGEPASSWORD_BUTTON" />
//                   </Link>
//                 </div>
//                 <div style={{textAlign:'center'}}>
//                   <button
//                     id="kt_login_signin_submit"
//                     type="submit"
//                     disabled={isSubmitting}
//                     className={`btn btn-primary btn-elevate kt-login__btn-primary ${clsx(
//                       {
//                         "kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light": loading
//                       }
//                     )}`}
//                     style={loadingButtonStyle}
//                   >
//                     Sign In
//                   </button>
//                   </div>
// {/* 
//                 <div style={{display:'flex',justifyContent:'center',marginTop:'10%'}}>
//                   <div className="kt-login__head">
//                     <span className="kt-login__signup-label">
//                       Don't have an account yet?
//                     </span>
//                     &nbsp;&nbsp;
//                   <Link to="/auth/registration" className="kt-link kt-login__signup-link">
//                     Sign Up!
//                   </Link>
//                 </div>
//                 </div> */}
//                 {/* </div> */}
//               </form>
//             )}
//           </Formik>

//           {/* <div className="kt-login__divider">
//             <div className="kt-divider">
//               <span />
//               <span>OR</span>
//               <span />
//             </div>
//           </div>

//           <div className="kt-login__options">
//             <Link to="http://facebook.com" className="btn btn-primary kt-btn">
//               <i className="fab fa-facebook-f" />
//               Facebook
//             </Link>
//             <Link to="http://twitter.com" className="btn btn-info kt-btn">
//               <i className="fab fa-twitter" />
//               Twitter
//             </Link>
//             <Link to="google.com" className="btn btn-danger kt-btn">
//               <i className="fab fa-google" />
//               Google
//             </Link>
//           </div> */}
//         </div>
//       </div>
//     </>
//   );
// }

// export default injectIntl(
//   connect(
//     null,
//     auth.actions
//   )(Login)
// );


import React,{useState, createContext} from 'react'
import { Link } from "react-router-dom";
import { Formik } from "formik";
import * as Yup from 'yup'
import ErrorValidation from '../errors/ErrorValidation';
import { TextField,Input,IconButton,InputAdornment ,InputLabel,FormControl,Checkbox,FormControlLabel} from "@material-ui/core";
import {Visibility,VisibilityOff} from '@material-ui/icons';
import clsx from "clsx";
import cogoToast from 'cogo-toast';
import axios from "axios";

export const LoginContext = createContext()
export default function Login() {
  // console.log(axios.post('api/auth/login'))

  const [loading, setLoading] = useState(false);
  const [remember,setRemember] =useState(false);
  const [isAuthenticated,setIsAuthenticated]=useState(false);
  const [token,setToken]=useState(undefined)

  const [icon,setIcon]=useState(false)
  const [loadingButtonStyle, setLoadingButtonStyle] = useState({
    paddingRight: "2.5rem"
  });
  
  const enableLoading = () => {
    setLoading(true);
    setLoadingButtonStyle({ paddingRight: "3.5rem" });
  };

  const disableLoading = () => {
    setLoading(false);
    setLoadingButtonStyle({ paddingRight: "2.5rem" });
  };

  const validationSchema = Yup.object().shape({
    email:Yup.string()
    .email("Must be a valid email address")
    .max(255,"Must be shorter than 255")
    .required("Email address is required"),
    password:Yup.string().required("Password is required")
  });

  const handleClickShowPassword = () => {
    setIcon(!icon);
  };

  const handleMouseDownPassword = event => {
    event.preventDefault();
  };

  const rememberme = e=>{
    setRemember(!remember)
  }

  const loginContext ={token:token,isAuthenticated:isAuthenticated}

  return (
    <LoginContext.Provider value={loginContext}>
    <div className="kt-login__body">
      <div className="kt-login__form">
        <div className="kt-login__title">
          <h3>Login</h3>
        </div>

        <Formik
          initialValues={{email:'',password:''}}
          validationSchema={validationSchema}
          onSubmit={(values, { setStatus, setSubmitting }) => {
            enableLoading();
            setSubmitting(true)
            setTimeout(() => {
              fetch('http://173.249.49.7:9020/api/signIn', {
                  method: "POST",
                  headers: {
                    "Content-Type": "application/json"
                  },
                  body: JSON.stringify({
                    email: values.email,
                    password: values.password
                  })
                })
                .then((response) => response.json())
                .then(res => {
                  const accessToken = res.token;
                  if(accessToken !== undefined){
                    window.localStorage.setItem("apitoken", accessToken);
                    setIsAuthenticated(!isAuthenticated)
                    setToken(accessToken)
                    disableLoading()
                    setTimeout(()=>{
                      window.location.reload()
                    },500)
                  }else{
                    disableLoading();
                    setSubmitting(false)
                    cogoToast.error('Invalid email or password')
                  }
                })
                  .catch(error => {
                    disableLoading();
                    setSubmitting(false);
                    setStatus(error)
                  } );
            }, 1000);
          }}
        >
          {({values,errors,touched,handleChange,handleBlur,handleSubmit,isSubmitting})=>(
            <form
              autoComplete="off"
              className="kt-form"
              onSubmit={handleSubmit}
            >
              <div className="form-group">
                <TextField
                  type="email"
                  label="Email"
                  margin="normal"
                  className={touched.email && errors.email ? "has-error kt-width-full":"kt-width-full"}
                  name="email"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.email}
                />
              </div>
              <ErrorValidation touched={touched.email} message={errors.email} />

              <div className="form-group">
              <FormControl
                  style={{width:'100%'}}>
                <InputLabel htmlFor="standard-adornment-password">Password</InputLabel>
                <Input
                  type={icon ? 'text' : 'password'}
                  // label="Password"
                  name="password"
                  className="kt-width-full"
                  value={values.password}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  endAdornment={
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={handleClickShowPassword}
                        onMouseDown={handleMouseDownPassword}
                      >
                        {icon ? <Visibility /> : <VisibilityOff />}
                      </IconButton>
                    </InputAdornment>
                  }
                />
                </FormControl>
              </div>
              <ErrorValidation touched={touched.password} message={errors.password} />

              <div className="flexSpacebetween" style={{marginBottom:"20px"}} >
                <FormControlLabel
                  control={
                    <Checkbox checked={remember} onChange={rememberme} style={{color:"#5867dd"}}/>
                  }
                  label="Remember me"
                />

                <Link
                  to="/auth/forgot-password"
                  className="kt-link kt-login__link-forgot"
                >
                forgot password
                </Link>

              </div>
                <div style={{textAlign:'center'}}>
                  <button
                    id="kt_login_signin_submit"
                    type="submit"
                    disabled={isSubmitting}
                    className={`btn btn-primary btn-elevate kt-login__btn-primary ${clsx(
                      {
                        "kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light": loading
                      }
                    )}`}
                    style={loadingButtonStyle}
                  >
                    Sign In
                  </button>
                  </div>
            </form>
          )}
        </Formik>
      </div>
    </div>
    </LoginContext.Provider>
  )
}
