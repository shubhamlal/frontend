import React, { useState, Component } from 'react'
import { Formik } from "formik";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { FormattedMessage, injectIntl } from "react-intl";
import { Checkbox, FormControlLabel, TextField, IconButton, InputAdornment } from "@material-ui/core";
import * as auth from "../../store/ducks/auth.duck";
import { register } from "../../crud/auth.crud";
import { Visibility, VisibilityOff } from '@material-ui/icons';

function ChangePassword(props) {
    const { intl } = props;
    const [icon, setIcon] = useState(false)
    const [icon3, setIcon3] = useState(false)
    const [icon2, setIcon2] = useState(false)
    const handleClickShowoldPassword = () => {
        setIcon3(!icon3);
    };
    const handleClickShowPassword = () => {
        setIcon(!icon);
    };
    const handleClickShowConfirmPassword = () => {
        setIcon2(!icon2);
    };

    const handleMouseDownPassword = event => {
        event.preventDefault();
    };
    return (
        <div className="kt-login__body">
            <div className="kt-login__form">
                <div className="kt-login__title">
                    <h3>
                        <FormattedMessage id="AUTH.CHANGEPASSWORD.TITLE" />
                    </h3>
                </div>

                <Formik
                    initialValues={{
                        newPassword: "",
                        oldPassword: "",
                        confirmPassword: "",

                    }}
                    validate={values => {
                        const errors = {};


                        if (!values.oldPassword) {
                            errors.oldPassword = intl.formatMessage({
                                id: "AUTH.VALIDATION.REQUIRED_FIELD"
                            });
                        }
                        if (!values.newPassword) {
                            errors.newPassword = intl.formatMessage({
                                id: "AUTH.VALIDATION.REQUIRED_FIELD"
                            });
                        }
                        else if (0 < values.newPassword.length < 6) {
                            errors.password = intl.formatMessage({
                                id: "AUTH.VALIDATION.MIN_LENGTH_FIELD"
                            });
                        }

                        if (!values.confirmPassword) {
                            errors.confirmPassword = intl.formatMessage({
                                id: "AUTH.VALIDATION.REQUIRED_FIELD"
                            });
                        } else if (values.newPassword !== values.confirmPassword) {
                            errors.confirmPassword =
                                "Password and Confirm Password didn't match.";
                        }



                        return errors;
                    }}
                // onSubmit={(values, { setStatus, setSubmitting }) => {
                //     register(
                //         values.oldpassword,
                //         values.newPassword,
                //         values.confirmPassword
                //     )
                //         .then(({ data: { accessToken } }) => {
                //             props.register(accessToken);
                //         })
                //         .catch(() => {
                //             setSubmitting(false);
                //             setStatus(
                //                 intl.formatMessage({
                //                     id: "AUTH.VALIDATION.INVALID_LOGIN"
                //                 })
                //             );
                //         });
                // }}
                >
                    {({
                        values,
                        status,
                        errors,
                        touched,
                        handleChange,
                        handleBlur,
                        handleSubmit,
                        isSubmitting
                    }) => (
                            <form onSubmit={handleSubmit} className="kt-form">
                                {status && (
                                    <div role="alert" className="alert alert-danger">
                                        <div className="alert-text">{status}</div>
                                    </div>
                                )}
                                <div className="form-group mb-0">
                                    <TextField
                                        type={icon3 ? 'text' : 'password'}
                                        margin="normal"
                                        label="oldPassword"
                                        className="kt-width-full"
                                        name="oldPassword"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.oldPassword}

                                        helperText={touched.oldPassword && errors.oldPassword}
                                        error={Boolean(touched.oldPassword && errors.oldPassword)}
                                        InputProps={{
                                            endAdornment: <InputAdornment position="end">
                                                <IconButton
                                                    aria-label="toggle password visibility"
                                                    onClick={handleClickShowoldPassword}
                                                    onMouseDown={handleMouseDownPassword}
                                                >
                                                    {icon3 ? <Visibility /> : <VisibilityOff />}
                                                </IconButton>
                                            </InputAdornment>
                                        }}

                                    />
                                </div>

                                <div className="form-group mb-0">
                                    <TextField
                                        type={icon ? 'text' : 'password'}
                                        margin="normal"
                                        label="newPassword"
                                        className="kt-width-full"
                                        name="newPassword"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.newPassword}

                                        helperText={touched.newPassword && errors.newPassword}
                                        error={Boolean(touched.newPassword && errors.newPassword)}
                                        InputProps={{
                                            endAdornment: <InputAdornment position="end">
                                                <IconButton
                                                    aria-label="toggle password visibility"
                                                    onClick={handleClickShowPassword}
                                                    onMouseDown={handleMouseDownPassword}
                                                >
                                                    {icon ? <Visibility /> : <VisibilityOff />}
                                                </IconButton>
                                            </InputAdornment>
                                        }}

                                    />
                                </div>

                                <div className="form-group">
                                    <TextField
                                        type={icon2 ? 'text' : 'password'}
                                        margin="normal"
                                        label="Confirm Password"
                                        className="kt-width-full"
                                        name="confirmPassword"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.confirmPassword}
                                        helperText={touched.confirmPassword && errors.confirmPassword}
                                        error={Boolean(
                                            touched.confirmPassword && errors.confirmPassword
                                        )}
                                        InputProps={{
                                            endAdornment: <InputAdornment position="end">
                                                <IconButton
                                                    aria-label="toggle password visibility"
                                                    onClick={handleClickShowConfirmPassword}
                                                    onMouseDown={handleMouseDownPassword}
                                                >
                                                    {icon2 ? <Visibility /> : <VisibilityOff />}
                                                </IconButton>
                                            </InputAdornment>
                                        }}
                                    />
                                </div>



                                <div className="kt-login__actions">


                                    <Link to="/auth">
                                        <button type="button" className="btn btn-secondary btn-elevate kt-login__btn-secondary">
                                            Back
                  </button>
                                    </Link>

                                    <button
                                        type="submit"
                                        disabled={isSubmitting}
                                        className="btn btn-primary btn-elevate kt-login__btn-primary"
                                    >
                                        Submit
                </button>
                                </div>
                            </form>
                        )}
                </Formik>
            </div>
        </div>
    );
}



export default injectIntl(
    connect(
        null
        // auth.actions
    )(ChangePassword)
);
