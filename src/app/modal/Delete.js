import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import { Button,Fade,Backdrop,Modal } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    modal: {
      display: 'flex',
      justifyContent:'center',
      background:'rgba(0,0,0,0.6)',
      alignItems:'baseline',
      padding:'25px'
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    },
  }));
export default function Delete(props) {
    const classes = useStyles();
    return (
        <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={classes.modal}
            open={props.open}
            onClose={props.handleClick}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
            timeout: 500,
            }}
        >
        <Fade in={props.open}>
          <div className={classes.paper}>
            <p>{props.confirmMsg}</p>
            <div className="flexEnd">
                <Button color="primary" onClick={props.clickConfirm}>Yes</Button>
                <Button variant="contained" color="primary" onClick={props.handleClick}>No</Button>
            </div>
          </div>
        </Fade>
      </Modal>
    )
}
