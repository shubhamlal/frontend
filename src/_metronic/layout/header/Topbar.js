// import React from "react";
// import UserProfile from "../../../app/partials/layout/UserProfile";
// import { Button } from "@material-ui/core";

// export default class Topbar extends React.Component {
//   render() {
//     return (
//       <div className="kt-header__topbar">
//         {/* <UserProfile showAvatar={true} showHi={true} showBadge={false} /> */}
//         <Button color="primary" onClick={logout}>Sign Out</Button>
//       </div>
//     );
//   }
// }

import React from 'react'
import { Button } from '@material-ui/core'

export default function Topbar() {
  const logout = ()=>{
    window.localStorage.removeItem('apitoken')
    window.location.reload()
  }
  return (
    <div className="kt-header__topbar">
      <Button color="primary" onClick={logout}>Signout</Button>
    </div>
  )
}

