export default {
  // header: {
  //   self: {},
  //   items: [
      
  //   ]
  // },
  aside: {
    self: {},
    items: [
      {
        title:'Dashboard',
        root:true,
        bullet:"dot",
        icon:"flaticon2-browser-2",
        page:"dashboard"
      },
      {
        title: "Product",
        root: true,
        bullet: "dot",
        icon: "flaticon2-browser-2",
          submenu: [
            {
              title: "Bulk Upload Product",
              page: "upload-product"
            },
            {
              title: "Upload Design",
              page: "upload-design"
            },
            {
              title: "Show Product",
              page: "products"
            },
            {
              title: "Category",
              page: "category"
            },
            {
              title: "Sub-Category",
              page: "subCategory"
            },
          ]
        },
        {
          title:"Bulk Upload Report",
          root:true,
          bullet:'dot',
          icon:'flaticon2-browser-2',
          page:'bulkUploadReport'
        },
        {
          title: "User Managment",
          root: true,
          bullet: "dot",
          icon: "flaticon2-browser-2",
            submenu: [
              {
                title: "Merchant Details",
                page: "merchant-details"
              },
              {
                title: "Broker Details",
                page: "broker-details"
              },
              {
                title: "Catalog Permission",
                page: "catalog-permission"
              },
              // {
              //   title: "Roles & Permission",
              //   page: "roles-permission"
              // }
            ]
          },
          {
            title: "CSM",
            root: true,
            bullet: "dot",
            icon: "flaticon2-browser-2",
              submenu: [
                {
                  title: "Email Alert",
                  page: "email-alert"
                },
                {
                  title: "SMS Alert",
                  page: "sms-alert"
                },
                
              ]
            },
            {
              title: "Config Details",
              root: true,
              bullet: "dot",
              icon: "flaticon2-browser-2",
                submenu: [
                  {
                    title: "Create Vle Id",
                    page: "create-vle-id"
                  },
                  {
                    title: "Wallet Price",
                    page: "wallet-price"
                  },
                  {
                    title: "Admin Log",
                    page: "admin-log"
                  },
                  {
                    title: "Change Password",
                    page: "changePassword"
                  },
                ]
              },
            ]
          }
        };
